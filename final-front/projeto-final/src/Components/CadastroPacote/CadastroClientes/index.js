import React, { Component } from 'react';

import Form from '../../../AntDComponents/Form';
import Button from '../../../AntDComponents/Button';
import Select from '../../../AntDComponents/Select';

import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class CadastrarCliente extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      clientes: [],
      clientesAdicionados: []
    }
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosClientes().then(e => {
      this.setState((state) => {
        return {
          ...state, clientes: e.data
        }
      });
    });
  }

  retornaDados = ( dados ) => {
    const { addCliente } = this.props;

    const newState = this.state.clientesAdicionados;
    newState.push( dados.id );

    this.setState((state) => {
      return {
        ...state, clientesAdicionados: newState
      }
    })

    addCliente( dados );
  }


  renderClientes = () => {
    const { Option } = Select;
    const clientesOption = []

    this.state.clientes.forEach(cliente => {
      clientesOption.push(<React.Fragment>
        {<Option value={cliente.id} disabled={this.state.clientesAdicionados.includes( cliente.id )}>{`${cliente.nome}, CPF: ${cliente.cpf} `}</Option>}
      </React.Fragment>)
    })
    return clientesOption;
  }

  render() {

    return (
      <Form
        labelCol={{
          span: 24,
        }}
        wrapperCol={{
          span: 24,
        }}
        layout="vertical"
        onFinish={this.retornaDados}
      >
        <Form.Item
          name="id"
          label="Cliente"
          rules={[{ required: true, message: "Escolha os clientes"},]}
        >
          <Select
            placeholder="Escolha o cliente"
            allowClear
          >
            {this.renderClientes()}
          </Select>
        </Form.Item>
        <Form.Item >
          <Button type="primary" htmlType="submit">Adicionar</Button>
        </Form.Item>

      </Form>
    );

  }
}