import React, { Component } from 'react';

import Form from '../../../AntDComponents/Form';
import Button from '../../../AntDComponents/Button';
import Select from '../../../AntDComponents/Select';

import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class CadastrarEspacos extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      espacos: [],
      espacosAdicionados: []
    }
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosEspacos().then(e => {
      this.setState((state) => {
        return {
          ...state, espacos: e.data
        }
      });
    });
  }

  retornaDados = (dados) => {
    const { addEspaco } = this.props;

    const newState = this.state.espacosAdicionados;
    newState.push(dados.id);

    this.setState((state) => {
      return {
        ...state, espacosAdicionados: newState
      }
    })

    addEspaco(dados);
  }


  renderEspacos = () => {
    const { Option } = Select;
    const espacosOption = []

    this.state.espacos.forEach(espaco => {
      espacosOption.push(<React.Fragment>
        {<Option value={espaco.id} disabled={this.state.espacosAdicionados.includes(espaco.id)}>{`${espaco.nome}, ${espaco.valor},  Capacidade: ${espaco.qtdPessoas}`}</Option>}
      </React.Fragment>)
    })
    return espacosOption;
  }

  render() {

    return (
      <Form
        labelCol={{
          span: 24,
        }}
        wrapperCol={{
          span: 24,
        }}
        layout="vertical"
        onFinish={this.retornaDados}
      >
        <Form.Item
          name="id"
          label="Espaco"
          rules={[{ required: true, message: "Escolha os espacos" },]}
        >
          <Select
            placeholder="Escolha o espaco"
            allowClear
          >
            {this.renderEspacos()}
          </Select>
        </Form.Item>
        <Form.Item >
          <Button type="primary" htmlType="submit">Adicionar</Button>
        </Form.Item>

      </Form>
    );

  }
}