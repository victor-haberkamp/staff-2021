import React, { Component } from 'react';

import Form from '../../../AntDComponents/Form';
import Input from '../../../AntDComponents/Input';
import Button from '../../../AntDComponents/Button';
import Select from '../../../AntDComponents/Select';

import tiposContratacao from '../../../Constants/tipoContratacao';

import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class CadastroPacotePacote extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
    }
  }

  retornaDados = (data) => {
    const { chamarApi } = this.props;
    chamarApi( data );
  }

  renderTiposContratacao = () => {
    const { Option } = Select;
    const tipos = []

    tiposContratacao.forEach(tipo => {
      tipos.push(<React.Fragment>
        {<Option value={tipo}>{tipo}</Option>}
      </React.Fragment>)
    })
    return tipos;
  }

  renderForm = () => {
    const { buttonDisplay } = this.props;
    return (
      <Form
      labelCol={{
        span: 24,
      }}
      wrapperCol={{ span: 24,
      }}
      layout="vertical"
        onFinish={this.retornaDados}
      >
        <Form.Item
          name="tipoContratacao"
          label="Tipo De Contratação"
          rules={[{ required: true, },]}
        >
          <Select
            placeholder="Escolha o tipo de contratação"
            allowClear
          >
            {this.renderTiposContratacao()}
          </Select>
        </Form.Item>

        <Form.Item
          label="Quantidade"
          name="quantidade"
          rules={[{ required: true, message: "Insira a quantidade" },]}
        >
          <Input placeholder="Insira a quantidade" />
        </Form.Item>

        <Form.Item
          label="Prazo"
          name="prazo"
          rules={[{ required: true, message: "Insira o prazo" },]}
        >
          <Input placeholder="Insira o prazo" />
        </Form.Item>

        <Form.Item
          label="Prazo"
          name="prazo"
          rules={[{ required: true, message: "Insira o prazo" },]}
        >
          <Input placeholder="Insira o prazo" />
        </Form.Item>

        <Form.Item
          label="Quantidade de contratações do pacote"
          name="quantidadeClientes"
          rules={[{ required: true, message: "Insira a quantidade" },]}
        >
          <Input placeholder="Insira a quantidade" />
        </Form.Item>

        <Form.Item
          label='Valor'
          name="valor"
          rules={[{ required: true, message: "Insira o valor" },]}
        >
          <Input placeholder="Insira o valor" />
        </Form.Item>

        { buttonDisplay ==='loading' && 
        <Form.Item >
          <Button type="primary" loading>Cadastrando</Button>
        </Form.Item> }

        { buttonDisplay ==='cadastrar' && 
        <Form.Item >
          <Button type="primary" htmlType="submit">Cadastrar</Button>
        </Form.Item> }

        { buttonDisplay ==='erro' && 
        <Form.Item >
          <Button type="danger" htmlType="submit">Verifique os dados</Button>
        </Form.Item> }

        { buttonDisplay ==='sucesso' && 
        <Form.Item >
          <Button type="primary" disabled>Cadastrado com sucesso</Button>
        </Form.Item> }
      </Form>
    );
  }

  render() {

    return (
      this.renderForm()
    );
  }

}
