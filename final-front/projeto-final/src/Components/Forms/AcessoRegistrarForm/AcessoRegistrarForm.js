import React, { Component } from 'react';

import Form from '../../../AntDComponents/Form';
import Switch from '../../../AntDComponents/Switch';
import Button from '../../../AntDComponents/Button';
import Select from '../../../AntDComponents/Select';

import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class CadastrarContratacaoForm extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      clientes: [],
      espacos: []
    }
  }

  retornaDados = (data) => {
    const { chamarApi } = this.props;
    chamarApi( this.formatarData(data));
  }

  formatarData = (data) => {
    const dataJson = {
      espacoId: data.espacoId,
      clienteId: data.clienteId
    }
    dataJson.isEntrada =  data.saida ? false : true;

    return dataJson;
  }

  componentDidMount() {
    const requisicoes = [this.coworkingApi.buscarTodosClientes(), this.coworkingApi.buscarTodosEspacos()]
    Promise.all(requisicoes).then(e => {
      this.setState((state) => {
        return {
          ...state, clientes: e[0].data, espacos: e[1].data
        }
      });
    });
  }

renderClientes = () => {
  const { Option } = Select;
  const clientesOption = []

  this.state.clientes.forEach(cliente => {
    clientesOption.push(<React.Fragment>
      {<Option value={cliente.id}>{`${cliente.nome}, CPF: ${cliente.cpf} `}</Option>}
    </React.Fragment>)
  })
  return clientesOption;
}

renderEspacos = () => {
  const { Option } = Select;
  const espacosOption = []

  this.state.espacos.forEach(espaco => {
    espacosOption.push(<React.Fragment>
      {<Option value={espaco.id}>{`${espaco.nome}, ${espaco.valor},  Capacidade: ${espaco.qtdPessoas}`}</Option>}
    </React.Fragment>)
  })
  return espacosOption;
}

renderForm = () => {

  const { buttonDisplay } = this.props;

  return (
    <Form
      labelCol={{
        span: 24,
      }}
      wrapperCol={{
        span: 24,
      }}
      layout="vertical"
      onFinish={this.retornaDados}
    >
      <Form.Item
        name="clienteId"
        label="Cliente"
        rules={[{ required: true, },]}
      >
        <Select
          placeholder="Escolha o cliente"
          allowClear
        >
          {this.renderClientes()}
        </Select>
      </Form.Item>

      <Form.Item
        name="espacoId"
        label="Espaço"
        rules={[{ required: true, },]}
      >
        <Select
          placeholder="Escolha o espaço"
          allowClear
        >
          {this.renderEspacos()}
        </Select>
      </Form.Item>

      <Form.Item label="Saída?" name="saida">
        <Switch />
      </Form.Item>

      {buttonDisplay === 'loading' &&
        <Form.Item >
          <Button type="primary" loading>Registrando</Button>
        </Form.Item>}

      {buttonDisplay === 'cadastrar' &&
        <Form.Item >
          <Button type="primary" htmlType="submit">Registrar</Button>
        </Form.Item>}

      {buttonDisplay === 'erro' &&
        <Form.Item >
          <Button type="danger" htmlType="submit">Verifique os dados</Button>
        </Form.Item>}

      {buttonDisplay === 'sucesso' &&
        <Form.Item >
          <Button type="primary" disabled>Registrado com sucesso</Button>
        </Form.Item>}
    </Form>
  );
}

render() {

  return (
    this.renderForm()
  );
}

}
