import React, { Component } from 'react';

import Form from '../../../AntDComponents/Form';
import Input from '../../../AntDComponents/Input';
import Button from '../../../AntDComponents/Button';
import Select from '../../../AntDComponents/Select';

import tiposContratacao from '../../../Constants/tipoContratacao';

import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class CadastrarContratacaoForm extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      clientes: [],
      espacos: []
    }
  }

  retornaDados = (data) => {
    const { chamarApi } = this.props;
    chamarApi(this.formatarData(data));
  }

  formatarData = (data) => {
    const dataJson = {
      espaco:{
        id: data.espaco
      },
      cliente:{
        id: data.cliente
      },
      tipoContratacao: data.tipoContratacao,
      quantidade: data.quantidade,
      prazo: data.prazo,
    }
    
    dataJson.desconto = data.desconto ? data.desconto : 0;  

    return dataJson;
  }

  componentDidMount() {
    const requisicoes = [this.coworkingApi.buscarTodosClientes(), this.coworkingApi.buscarTodosEspacos()]
    Promise.all(requisicoes).then(e => {
      this.setState((state) => {
        return {
          ...state, clientes: e[0].data, espacos: e[1].data
        }
      });
    });
  }

  renderTiposContratacao = () => {
    const { Option } = Select;
    const tipos = []

    tiposContratacao.forEach(tipo => {
      tipos.push(<React.Fragment>
        {<Option value={tipo}>{tipo}</Option>}
      </React.Fragment>)
    })
    return tipos;
  }

  renderClientes = () => {
    const { Option } = Select;
    const clientesOption = []

    this.state.clientes.forEach(cliente => {
      clientesOption.push(<React.Fragment>
        {<Option value={cliente.id}>{`${cliente.nome}, CPF: ${cliente.cpf} `}</Option>}
      </React.Fragment>)
    })
    return clientesOption;
  }

  renderEspacos = () => {
    const { Option } = Select;
    const espacosOption = []

    this.state.espacos.forEach(espaco => {
      espacosOption.push(<React.Fragment>
        {<Option value={espaco.id}>{`${espaco.nome}, ${espaco.valor},  Capacidade: ${espaco.qtdPessoas}`}</Option>}
      </React.Fragment>)
    })
    return espacosOption;
  }

  renderForm = () => {
    const { buttonDisplay } = this.props;
    return (
      <Form
      labelCol={{
        span: 24,
      }}
      wrapperCol={{ span: 24,
      }}
      layout="vertical"
        onFinish={this.retornaDados}
      >
        <Form.Item
          name="cliente"
          label="Cliente"
          rules={[{ required: true, },]}
        >
          <Select
            placeholder="Escolha o cliente"
            allowClear
          >
            {this.renderClientes()}
          </Select>
        </Form.Item>

        <Form.Item
          name="espaco"
          label="Espaço"
          rules={[{ required: true, },]}
        >
          <Select
            placeholder="Escolha o espaço"
            allowClear
          >
            {this.renderEspacos()}
          </Select>
        </Form.Item>

        <Form.Item
          name="tipoContratacao"
          label="Tipo De Contratação"
          rules={[{ required: true, },]}
        >
          <Select
            placeholder="Escolha o tipo de contratação"
            allowClear
          >
            {this.renderTiposContratacao()}
          </Select>
        </Form.Item>

        <Form.Item
          label="Quantidade"
          name="quantidade"
          rules={[{ required: true, message: "Insira a quantidade" },]}
        >
          <Input placeholder="Insira a quantidade" />
        </Form.Item>

        <Form.Item
          label="Prazo"
          name="prazo"
          rules={[{ required: true, message: "Insira o prazo" },]}
        >
          <Input placeholder="Insira o prazo" />
        </Form.Item>

        <Form.Item
          label="Desconto"
          name="desconto"
        >
          <Input placeholder="Opcional" />
        </Form.Item>

        { buttonDisplay ==='loading' && 
        <Form.Item >
          <Button type="primary" loading>Cadastrando</Button>
        </Form.Item> }

        { buttonDisplay ==='cadastrar' && 
        <Form.Item >
          <Button type="primary" htmlType="submit">Cadastrar</Button>
        </Form.Item> }

        { buttonDisplay ==='erro' && 
        <Form.Item >
          <Button type="danger" htmlType="submit">Verifique os dados</Button>
        </Form.Item> }

        { buttonDisplay ==='sucesso' && 
        <Form.Item >
          <Button type="primary" disabled>Cadastrado com sucesso</Button>
        </Form.Item> }
      </Form>
    );
  }

  render() {

    return (
      this.renderForm()
    );
  }

}
