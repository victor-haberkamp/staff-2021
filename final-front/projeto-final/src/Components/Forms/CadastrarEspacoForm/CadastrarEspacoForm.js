import React, { Component } from 'react';
import Form from '../../../AntDComponents/Form';
import Input from '../../../AntDComponents/Input'
import Button from '../../../AntDComponents/Button'

export default class CadastrarEspacoForm extends Component {  

  cadastraEspaco = ( data ) => {
    const { chamarApi } = this.props;
    chamarApi( data );
  }

  render() {

    const { buttonDisplay } = this.props;

    return (
      <>
        <Form
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          layout="vertical"
          initialValues={{
          }}
          onFinish={ this.cadastraEspaco }
        >
          <Form.Item 
            label="Nome:" 
            name="nome"
            rules={[
              {
                required: true,
                message: 'Digite nome do novo espaco',
              },
            ]}>
            <Input placeholder="Espaço Sideral"/>
          </Form.Item>
          <Form.Item
          label="Capacidade"
          name="capacidade"
          rules={[
            {
              required: true,
              message: 'Digite a capacidade do espaco',
            },
          ]}
        >
          <Input placeholder="Numero de pessoas permitidas" type="number"/>
        </Form.Item>
        <Form.Item
          label="Valor"
          name="valor"
          rules={[
            {
              required: true,
              message: 'Insira o valor do espaço',
            },
          ]}
        >
          <Input placeholder="R$76.60"/>
        </Form.Item>

        { buttonDisplay ==='loading' && 
        <Form.Item >
          <Button type="primary" loading>Cadastrando</Button>
        </Form.Item> }

        { buttonDisplay ==='cadastrar' && 
        <Form.Item >
          <Button type="primary" htmlType="submit">Cadastrar</Button>
        </Form.Item> }

        { buttonDisplay ==='erro' && 
        <Form.Item >
          <Button type="danger" htmlType="submit">Verifique os dados</Button>
        </Form.Item> }

        { buttonDisplay ==='sucesso' && 
        <Form.Item >
          <Button type="primary" disabled>Cadastrado com sucesso</Button>
        </Form.Item> }

        </Form>
      </>
    );
  };
}
