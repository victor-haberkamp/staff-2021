import React, { Component } from 'react';
import Form from '../../../AntDComponents/Form';
import Select from '../../../AntDComponents/Select';
import Button from '../../../AntDComponents/Button'

import tiposPagamento from '../../../Constants/tipoPagamento';

import CoworkingAPI from '../../../Models/CoworkingAPI';

export default class RealizarPagamento extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      contratacoes: []
    }
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosContratacao().then(e => {
      this.setState((state) => {
        return {
          ...state, contratacoes: e.data
        }
      });
    });
  }

  cadastraEspaco = (data) => {
    const { chamarApi } = this.props;
    chamarApi(data);
  }

  renderContratacoes = () => {
    const { Option } = Select;
    const contratacaoOption = []

    this.state.contratacoes.forEach(contratacao => {
      contratacaoOption.push(<React.Fragment>
        {<Option value={contratacao.id}>{`CLIENTE: ${contratacao.cliente.nome}, CPF: ${contratacao.cliente.cpf}, ESPAÇO: ${ contratacao.espaco.nome }, Valor: R$${ contratacao.valorASerCobrado } `}</Option>}
      </React.Fragment>)
    })
    return contratacaoOption;
  }


  renderTiposPagamento = () => {
    const { Option } = Select;
    const tipos = []

    tiposPagamento.forEach(tipo => {
      tipos.push(<React.Fragment>
        {<Option value={tipo}>{tipo}</Option>}
      </React.Fragment>)
    })
    return tipos;
  }

  render() {

    const { buttonDisplay } = this.props;

    return (
      <>
        <Form
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          layout="vertical"
          initialValues={{
          }}
          onFinish={this.cadastraEspaco}
        >
          <Form.Item
            name="contratacaoId"
            label="Escolha a contratacao:"
            rules={[{ required: true, message: "Escolha a contratação"},]}
          >
            <Select
              placeholder="Escolha o espaço"
              allowClear
            >
              {this.renderContratacoes()}
            </Select>
          </Form.Item>

          <Form.Item
            name="tipoPagamento"
            label="Forma de pagamento"
            rules={[{ required: true, message: "Escolha o tipo de pagamento" },]}
          >
            <Select
              placeholder="Escolha a forma de pagamento"
              allowClear
            >
              {this.renderTiposPagamento()}
            </Select>
          </Form.Item>

          {buttonDisplay === 'loading' &&
            <Form.Item >
              <Button type="primary" loading>Cadastrando</Button>
            </Form.Item>}

          {buttonDisplay === 'cadastrar' &&
            <Form.Item >
              <Button type="primary" htmlType="submit">Cadastrar</Button>
            </Form.Item>}

          {buttonDisplay === 'erro' &&
            <Form.Item >
              <Button type="danger" htmlType="submit">Erro ao realizar pagamento</Button>
            </Form.Item>}

          {buttonDisplay === 'sucesso' &&
            <Form.Item >
              <Button type="primary" disabled>Cadastrado com sucesso</Button>
            </Form.Item>}

        </Form>
      </>
    );
  };
}
