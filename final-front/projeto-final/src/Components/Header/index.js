import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Header from '../../AntDComponents/Header';
import Menu from '../../AntDComponents/Menu';

import './header.css';

export default class HeaderUi extends Component {

  render() {

    return (
      <React.Fragment>
        <Header className="header" style={{ position: 'fixed', zIndex: 1, width: '100%', paddingLeft: 20 }}>
          <div className="logo"/>
          <Menu theme="dark" mode="horizontal">
            <h1 style={ {margin:0} }>{ <Link to='/' >Coworking Application</Link>}</h1>
          </Menu>         
        </Header>
      </React.Fragment>
    )

  }
}
