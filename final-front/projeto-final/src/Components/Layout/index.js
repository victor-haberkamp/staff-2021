import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import HeaderUi from '../Header';
import { UserOutlined, AppstoreAddOutlined, DollarOutlined, DeliveredProcedureOutlined } from '@ant-design/icons';
import Layout from '../../AntDComponents/Layout';
import Menu from '../../AntDComponents/Menu';


export default class MyLayout extends Component {

  render() {
    const { SubMenu } = Menu;
    const { Content, Sider } = Layout;
    const { children } = this.props;

    return (
      <Layout>
        <div style={{ height: '64px'}}><HeaderUi/></div>
        <Layout>
          <Sider width={200} style={{position:'fixed' , height: '90vh'}} className="site-layout-background">
            <Menu
              mode="inline"
              style={{ height: '100%', borderRight: 0 }}
            >
              <SubMenu key="sub1" icon={<UserOutlined />} title="Clientes">
                <Menu.Item key="1"><Link to='/clientes/cadastrar'>Cadastrar</Link></Menu.Item>
                <Menu.Item key="2"><Link to='/clientes/listar'>Listar</Link></Menu.Item>
                <Menu.Item key="3"><Link to='/clientes/saldos'>Saldos</Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" icon={<AppstoreAddOutlined />} title="Espacos">
                <Menu.Item key="5"><Link to='/espacos/cadastrar'>Cadastrar</Link></Menu.Item>
                <Menu.Item key="6"><Link to='/espacos/listar'>Listar</Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub3" icon={<DollarOutlined />} title="Pagamento">
                <Menu.Item key="9"><Link to='/pagamento/contratacao/cadastrar'>Cadastrar Contratação</Link></Menu.Item>
                <Menu.Item key="10"><Link to='/pagamento/contratacao/listar'>Listar Contratação</Link></Menu.Item>
                <Menu.Item key="11"><Link to='/pagamento/contratacao'>Pagar Contratação</Link></Menu.Item>
                <Menu.Item key="12"><Link to='/pagamento/pacote/cadastrar'>Cadastrar Pacote</Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub4" icon={<DeliveredProcedureOutlined />} title="Acesso">
                <Menu.Item key="13"><Link to='/acesso/registrar'>Registrar</Link></Menu.Item>
              </SubMenu>
            </Menu>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                marginLeft: 200,
                minHeight: '90vh',
                
              }}
            >
              { children }
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}