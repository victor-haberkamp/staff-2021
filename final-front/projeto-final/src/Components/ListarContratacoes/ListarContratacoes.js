import React, { Component } from 'react';

import Table from '../../AntDComponents/Table'

export default class ListarContracoes extends Component {

  renderList = () => {
    const { contratacoes } = this.props;

    const columns = [
      { title: 'Cliente', dataIndex: 'cliente', key: 'cliente' },
      { title: 'Espaco', dataIndex: 'espaco', key: 'espaco' },
      { title: 'Valor', dataIndex: 'valor', key: 'valor' },
      { title: 'Prazo', dataIndex: 'prazo', key: 'prazo' },
      { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade' },
      { title: 'Tipo Contratacção', dataIndex: 'tipoContratacao', key: 'tipoContratacao' }
    ];

    const data = [];

    contratacoes.forEach( contratacao => {
      data.push({
        key: contratacao.id,
        cliente: `${ contratacao.cliente.nome }, CPF: ${ contratacao.cliente.cpf }`,
        espaco: contratacao.espaco.nome,
        valor: contratacao.valorASerCobrado,
        prazo: contratacao.prazo,
        quantidade: contratacao.quantidade,
        tipoContratacao: contratacao.tipoContratacao
      });
    });

    return (
      <Table
        className="components-table-demo-nested"
        columns={columns}
        dataSource={data}
      />
    );
  }

  render() {

    return (
      this.renderList()
    );
  }
}
