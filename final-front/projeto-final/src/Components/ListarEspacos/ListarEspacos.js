import React, { Component } from 'react';

import Table from '../../AntDComponents/Table';

export default class ListarEspaco extends Component {

  renderList = () => {
    const { espacos } = this.props;

    const columns = [
      { title: 'Nome', dataIndex: 'name', key: 'name' },
      { title: 'Capacidade', dataIndex: 'capacidade', key: 'capacidade' },
      { title: 'Valor', dataIndex: 'valor', key: 'valor' }
    ];

    const data = [];

    espacos.forEach(espaco => {
      data.push({
        key: espaco.id,
        name: espaco.nome,
        capacidade: espaco.qtdPessoas,
        valor: espaco.valor
      });
    });

    return (
      <Table
        className="components-table-demo-nested"
        columns={columns}
        dataSource={data}
      />
    );
  }

  render() {

    return (
      this.renderList()
    );
  }

}
