import React, { Component } from 'react';

import Table from '../../AntDComponents/Table'

export default class ListarSaldos extends Component {

  renderList = () => {
    const { saldos } = this.props;

    const columns = [
      { title: 'Cliente', dataIndex: 'cliente', key: 'cliente' },
      { title: 'Espaco', dataIndex: 'espaco', key: 'espaco' },
      { title: 'Vencimento', dataIndex: 'vencimento', key: 'vencimento' },
      { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade' },
      { title: 'Tipo Contratacção', dataIndex: 'tipoContratacao', key: 'tipoContratacao' }
    ];

    const data = [];

    for (let i = 0; i < saldos.length; i++) {
      data.push({
        key: i,
        cliente: `${ saldos[i].cliente.nome }, CPF: ${ saldos[i].cliente.cpf }`,
        espaco: saldos[i].espaco.nome,
        quantidade: saldos[i].quantidade,
        vencimento: saldos[i].vencimento,
        tipoContratacao: saldos[i].tipoContratacao
      });

    }

    return (
      <Table
        className="components-table-demo-nested"
        columns={columns}
        dataSource={data}
      />
    );
  }

  render() {

    return (
      this.renderList()
    );
  }
}
