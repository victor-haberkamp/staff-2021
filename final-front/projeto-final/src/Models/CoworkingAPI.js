import axios from 'axios';



export default class CoworkingAPI {
  constructor() {
    this.endereco = 'http://localhost:8080';
    this.token = localStorage.getItem( 'bearerToken' );
    this.config = { headers: { Authorization: this.token } }
  }

  salvarUsuario( { login, senha } ) {
    return axios.post( `${this.endereco}/user/salvar`, { login, senha } );
  }

  logarUuario( { login, senha } ) {
    return axios.post( `${this.endereco}/login`, { login, senha } ).then( e => localStorage.setItem( 'bearerToken', e.headers.authorization ) );
  }

  salvarTipoContato( { object } ) {
    return axios.post( `${ this.endereco }/api/tipoContato/salvar`, { object }, this.config );
  }

  buscarTipoContato( ) {
    return axios.get( `${ this.endereco }/api/tipoContato/`, this.config );
  }

  cadastrarCliente( cliente ) {
    return axios.post( `${ this.endereco }/api/cliente/salvar`, cliente, this.config );
  }

  buscarTodosClientes() {
    return axios.get( `${ this.endereco }/api/cliente/`, this.config );
  }

  cadastrarEspaco( espaco ) {
    return axios.post( `${ this.endereco }/api/espacos/salvar`, espaco, this.config );
  }

  buscarTodosEspacos() {
    return axios.get( `${ this.endereco }/api/espacos/`, this.config ); 
  }

  registrarContratacao( data ) {
    return axios.post( `${ this.endereco }/api/contratacao/salvar`, data, this.config );
  }

  buscarTodosContratacao() {
    return axios.get( `${ this.endereco }/api/contratacao/`,  this.config );
  }

  registrarPagamento( data ) {
    return axios.post( `${ this.endereco }/api/pagamento/pagar`, data, this.config );
  }

  buscarTodosSaldoCliente() {
    return axios.get( `${ this.endereco }/api/saldoCliente/`,  this.config );
  }

  registrarAcesso( data ) {
    return axios.post( `${ this.endereco }/api/acesso/registrarAcesso`, data, this.config );
  }

  registrarPacote( data ) {
    return axios.post( `${ this.endereco }/api/pacote/salvar`, data, this.config );
  }

}
