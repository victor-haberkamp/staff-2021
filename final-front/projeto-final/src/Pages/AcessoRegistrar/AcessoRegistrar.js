import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import Layout from '../../Components/Layout';
import AcessoRegistrarForm from '../../Components/Forms/AcessoRegistrarForm';

export default class AcessoRegistrar extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      buttonDisplay: 'cadastrar'
    }
  }

  registrarAcesso = ( data ) => {
    this.coworkingApi.registrarAcesso( data ).then( e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'sucesso'
        }
      })
    }).catch( e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'erro'
        }
      })
    }
    );
  }

  render() {

    return (

      <div>
        <Layout>
          <AcessoRegistrarForm chamarApi={this.registrarAcesso} buttonDisplay={this.state.buttonDisplay}></AcessoRegistrarForm>
        </Layout>
      </div>

    );
  }

}

