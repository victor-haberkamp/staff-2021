import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import Layout from '../../Components/Layout';
import CadastrarClienteForm from '../../Components/Forms/CadastrarClienteForm';


export default class CadastrarCliente extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      buttonDisplay: 'cadastrar'
    }
  }

  cadastrarCliente = (cliente) => {
    this.setState((state) => {
      return {
        ...state, buttonDisplay: 'loading'
      }
    });
    const clienteJson = {
      nome: cliente.nome,
      cpf: cliente.cpf,
      dataDeNascimento: cliente.dataDeNascimento,
      contato: [
        {
          tipoContato: {
            nome: 'email'
          },
          valor: cliente.email
        },
        {
          tipoContato: {
            nome: 'telefone'
          },
          valor: cliente.telefone
        }
      ]
    }

    this.coworkingApi.cadastrarCliente(clienteJson).then(e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'sucesso'
        }
      })
    }
    ).catch(e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'erro'
        }
      })
    }
    );

  }

  render() {

    return (

      <div>
        <Layout>
          <CadastrarClienteForm chamarApi={this.cadastrarCliente} buttonDisplay={this.state.buttonDisplay} />
        </Layout>
      </div>

    );
  }

}
