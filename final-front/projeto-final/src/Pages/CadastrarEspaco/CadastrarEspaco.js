import React, { Component } from 'react';

import CoworkingAPI from '../../Models/CoworkingAPI';

import Layout from '../../Components/Layout';
import CadastrarEspacoForm from '../../Components/Forms/CadastrarEspacoForm';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      buttonDisplay: 'cadastrar'
    }
  }

  cadastrarEspaco = (espaco) => {
    this.setState((state) => {
      return {
        ...state, buttonDisplay: 'loading'
      }
    });
    const espacoJson = {
      nome: espaco.nome,
      qtdPessoas: espaco.capacidade,
      valor: espaco.valor,

    }

    this.coworkingApi.cadastrarEspaco(espacoJson).then(e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'sucesso'
        }
      })
    }
    ).catch(e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'erro'
        }
      })
    }
    );
  }

  render() {

    return (
      <div>
        <Layout>
          <CadastrarEspacoForm chamarApi={this.cadastrarEspaco} buttonDisplay={this.state.buttonDisplay}  ></CadastrarEspacoForm>
        </Layout>

      </div>

    );
  }

}
