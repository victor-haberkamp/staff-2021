import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';
import CadastroClientes from '../../Components/CadastroPacote/CadastroClientes';
import CadastroEspacos from '../../Components/CadastroPacote/CadastroEspacos';
import CadastroPacotePacote from '../../Components/CadastroPacote/CadastroPacotePacote';

import Layout from '../../Components/Layout';


export default class CadastrarPacote extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.clientes = [];
    this.espacos = [];
    this.state = {
      buttonDisplay: 'cadastrar'
    }
  }
  /*
    registrarCadastro = ( data ) => {
      this.coworkingApi.registrarContratacao( data ).then(e => {
        console.log( e )
        this.setState((state) => {
          return {
            ...state, buttonDisplay: 'sucesso'
          }
        })
      }
      ).catch(e => {
        this.setState((state) => {
          return {
            ...state, buttonDisplay: 'erro'
          }
        })
      }
      );
    }
  */

  adicionarPacote = ( data ) => {
    this.coworkingApi.registrarPacote( { valor: data.valor } ).then( e => {
      console.log(e.data)
    });
  }


  adicionarCliente = (clienteId) => {
    this.clientes.push(clienteId);
    console.log(this.clientes)
  }

  adicionarEspaco = (espacoId) => {
    this.espacos.push(espacoId);
    console.log(this.espacos)
  }


  render() {

    return (

      <div>
        <Layout>
          <CadastroClientes addCliente={this.adicionarCliente}></CadastroClientes>
          <CadastroEspacos addEspaco={this.adicionarCliente}></CadastroEspacos>
          <CadastroPacotePacote chamarApi={this.adicionarPacote} buttonDisplay={this.state.buttonDisplay}></CadastroPacotePacote>
        </Layout>
      </div>

    );
  }

}
