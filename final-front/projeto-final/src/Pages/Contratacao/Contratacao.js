import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';
import CadastrarContratacaoForm from '../../Components/Forms/CadastrarContratacaoForm';

import Layout from '../../Components/Layout';


export default class CadastrarCliente extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      buttonDisplay: 'cadastrar'
    }
  }

  registrarCadastro = ( data ) => {
    this.coworkingApi.registrarContratacao( data ).then(e => {
      console.log( e )
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'sucesso'
        }
      })
    }
    ).catch(e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'erro'
        }
      })
    }
    );
  }

  render() {

    return (

      <div>
        <Layout>
          <CadastrarContratacaoForm chamarApi={this.registrarCadastro} buttonDisplay={this.state.buttonDisplay}></CadastrarContratacaoForm>
        </Layout>
      </div>

    );
  }

}
