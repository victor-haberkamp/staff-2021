import React, { Component } from 'react';
import { Redirect } from "react-router-dom";

import Layout from '../../Components/Layout';
import Button from '../../AntDComponents/Button';

import { UserDeleteOutlined } from '@ant-design/icons'

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      redirect: null
    }
  }

  sair = () => {
    localStorage.removeItem('usuario');
    localStorage.removeItem('bearerToken');
    this.setState((state) => {
      return {
        ...state, redirect: '/login'
      }
    }); 
  }

  render() {

    if ( !(this.state.redirect === null) ) {
      return <Redirect to={this.state.redirect} />
    }

    return (

      <div className="App">
        <Layout>
          <h1>Seja bem-vindo a mais um dia de trabalho!</h1>
          <Button type="primary" icon={<UserDeleteOutlined />}style={ {paddingLeft: 40, paddingRight: 40} } onClick={ this.sair }>Sair</Button>
        </Layout>
      </div>

    );
  }
}
