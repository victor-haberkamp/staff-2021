import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';


import Layout from '../../Components/Layout';
import ShowClientes from '../../Components/ShowClientes'


export default class ListarClientes extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      clientes: []
    }

  }

  buscarClientes() {
    this.coworkingApi.buscarTodosClientes().then( e => {
      this.setState((state) => {
        return {
          ...state, clientes: e.data
        }
      });
    });
  }

  render() {

    if( this.state.clientes.length === 0 ){
      this.buscarClientes();
    }

    return (

      <div>
        <Layout>
          <ShowClientes clientes={ this.state.clientes }/>
        </Layout>
      </div>

    );
  }

}
