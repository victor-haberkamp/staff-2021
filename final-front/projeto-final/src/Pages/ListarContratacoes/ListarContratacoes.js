import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import Layout from '../../Components/Layout';
import ListarContratacoes from '../../Components/ListarContratacoes';

export default class ListarEspaco extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      contratacoes: []
    }
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosContratacao().then(e => {
      this.setState((state) => {
        return {
          ...state, contratacoes: e.data
        }
      });
    });
  }

  render() {

    return (

      <div>
        <Layout>
          <ListarContratacoes contratacoes={ this.state.contratacoes } ></ListarContratacoes>
        </Layout>
      </div>

    );
  }

}