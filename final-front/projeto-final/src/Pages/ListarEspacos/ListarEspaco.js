import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import Layout from '../../Components/Layout';
import ListarEspacos from '../../Components/ListarEspacos';

export default class ListarEspaco extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      espacos: []
    }
  }

  buscarClientes() {
    this.coworkingApi.buscarTodosEspacos().then(e => {
      console.log( e )
      this.setState((state) => {
        return {
          ...state, espacos: e.data
        }
      });
    });
  }



  render() {
    
    if( this.state.espacos.length === 0 ){
      this.buscarClientes();
    }


    return (

      <div>
        <Layout>
          <ListarEspacos espacos={ this.state.espacos } ></ListarEspacos>
        </Layout>
      </div>

    );
  }

}