import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import Layout from '../../Components/Layout';
import ListarSaldos from '../../Components/ListarSaldos';

export default class ListarSaldosClientes extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      saldos: []
    }
  }

  componentDidMount() {
    this.coworkingApi.buscarTodosSaldoCliente().then(e => {
      this.setState((state) => {
        return {
          ...state, saldos: e.data
        }
      });
    });
  }

  render() {

    return (

      <div>
        <Layout>
            <ListarSaldos saldos={ this.state.saldos } ></ListarSaldos>
        </Layout>
      </div>

    );
  }

}

