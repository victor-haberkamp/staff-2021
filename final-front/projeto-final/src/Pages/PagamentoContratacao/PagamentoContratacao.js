import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';
import RealizarPagamento from '../../Components/Forms/RealizarPagamento';

import Layout from '../../Components/Layout';


export default class PagementoContratacao extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      buttonDisplay: 'cadastrar'
    }
  }

  realizarPagamento = ( data ) => {
    this.coworkingApi.registrarPagamento( data ).then(e => {
      console.log( e )
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'sucesso'
        }
      })
    }
    ).catch(e => {
      this.setState((state) => {
        return {
          ...state, buttonDisplay: 'erro'
        }
      })
    }
    );
  }


  render() {

    return (

      <Layout>
        <RealizarPagamento chamarApi={this.realizarPagamento} buttonDisplay={this.state.buttonDisplay} ></RealizarPagamento>
      </Layout>

    );

  }

}