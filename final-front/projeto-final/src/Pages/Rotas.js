import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';

import Home from './Home'
import Login from './Login'
import CadastrarCliente from './CadastrarCliente';
import ListarClientes from './ListarClientes';
import ListarSaldos from './ListarSaldos';
import CadastrarEspaco from './CadastrarEspaco';
import ListarEspaco from './ListarEspacos';
import Contratacao from './Contratacao';
import PagementoContratacao from './PagamentoContratacao';
import ListarContracoes from './ListarContratacoes';
import AcessoRegistrar from './AcessoRegistrar';
import CadastrarPacote from './CadastrarPacote';

import { RotasPrivadas } from '../Components/RotasPrivadas';

export default class Rotas extends Component {

  render() {
    return (
      <Router>
        <Route path="/login" exact component={ Login } />
        <RotasPrivadas path="/" exact component={ Home } />
        <RotasPrivadas path="/clientes/cadastrar" exact component={ CadastrarCliente } />
        <RotasPrivadas path="/clientes/listar" exact component={ ListarClientes } />
        <RotasPrivadas path="/clientes/saldos" exact component={ ListarSaldos } />
        <RotasPrivadas path="/espacos/cadastrar" exact component={ CadastrarEspaco } />
        <RotasPrivadas path="/espacos/listar" exact component={ ListarEspaco } />
        <RotasPrivadas path="/pagamento/contratacao/cadastrar" exact component={ Contratacao } />
        <RotasPrivadas path="/pagamento/contratacao/listar" exact component={ ListarContracoes } />
        <RotasPrivadas path="/pagamento/contratacao" exact component={ PagementoContratacao } />
        <RotasPrivadas path="/pagamento/pacote/cadastrar" exact component={ CadastrarPacote } />
        <RotasPrivadas path="/acesso/registrar" exact component={ AcessoRegistrar } />
      </Router>
    );
  }
}
