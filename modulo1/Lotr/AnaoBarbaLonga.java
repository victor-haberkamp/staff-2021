public class AnaoBarbaLonga extends Anao{
    
    public AnaoBarbaLonga(String nome) {
        super(nome);
    }
    
    @Override
    protected void sofrerDano() {
        int chanceSofrerDano = DadoD6.sortear();
        
        if( super.podeSofrerDano() && chanceSofrerDano > 2) {
            this.vida -= this.vida >= this.qtdDano ? this.qtdDano : 0;
            this.status = super.validacaoStatus();
        }else{
            this.status = super.validacaoStatus( Status.NAO_SOFREU_DANO );
        }
        
    }
}
