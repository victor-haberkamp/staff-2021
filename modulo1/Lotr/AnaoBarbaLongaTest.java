
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AnaoBarbaLongaTest{
    
    @Test
    public void anaoSofreDano10VezEVerificaSeTeveAlgumaVezQueNaoSofreuDano(){
        AnaoBarbaLonga anaoBarba = new AnaoBarbaLonga("Anao Viking");  
        double testeVida = 110.0;
        boolean testeVidaSofreuDano = false;
        
        for ( int i = 0; i < 10; i++ ) {
            anaoBarba.sofrerDano();
            testeVida -= 10.0;
            testeVidaSofreuDano = (anaoBarba.getVida() != testeVida);
        }
        assertTrue(testeVidaSofreuDano);
    }
    
}
