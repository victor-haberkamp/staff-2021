<<<<<<< HEAD
public class Elfo extends Personagem{
    private static int qtdElfos; 
    
    public Elfo( String nome ){
        super(nome);
        this.vida = 100.0;
        super.ganharItem( new Item(2, "Flecha") );
        super.ganharItem( new Item(1,"Arco") );
        Elfo.qtdElfos++;
    } 
    
    public static int getQtdElfos(){
        return qtdElfos;
=======
public class Elfo extends Personagem implements Atacante{
    private int indiceFlecha;
    private static int qtdElfos;
    
    {
        this.indiceFlecha = 1;
        Elfo.qtdElfos = 0;
    }
    
    public Elfo( String nome ) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item( 1, "Arco" ));
        this.inventario.adicionar(new Item( 2, "Flecha" ));
        Elfo.qtdElfos++;
    }
    
    public static int getQuantidade() {
        return Elfo.qtdElfos;
>>>>>>> otherrep/hipotese-alternativa
    }
    
    public void finalize() throws Throwable {
        Elfo.qtdElfos--;
<<<<<<< HEAD
    }    
    
    public Item getFlecha(){
        return this.inventario.buscar("Flecha");
    }
    
    private int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }   

    protected void diminuiUmaFlecha(){
        this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
    }
    
    protected boolean podeAtirar(){
        return this.getQtdFlecha() > 0;
    }
    
    public void  atirarFlecha( Dwarf anao ){
        if( this.podeAtirar() ){
            this.diminuiUmaFlecha();
            super.aumentarXP();
            super.sofrerDano();
            anao.sofrerDano();
        }     
    }
    
    public void atirarMultiplasFlechas ( int qtdFlechas, Dwarf anao){
        for( int i=0 ; i < qtdFlechas ; i++ ) {
            this.atirarFlecha( anao );
        }
    }   
    
    public String imprimirNomeClasse(){
        return "Elfo";
    } 
=======
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    private int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    private boolean podeAtirar() {
        return this.getQtdFlecha() > 0;
    }
    
    public void atirarFlecha( Anao anao ) {
        if( this.podeAtirar() ) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXP();
            super.sofrerDano();
            anao.sofrerDano();
        }
    }
    
    @Override
    public void atacar( Anao anao ) {
        this.atirarFlecha(anao);
    }
    
    public void atirarMultiplasFlechas( int vezes, Anao anao ) {
        for( int i = 0; i < vezes; i++ ){
            this.atirarFlecha(anao);
        }
    }
>>>>>>> otherrep/hipotese-alternativa
}