import java.util.*;

public class ElfoDeLuz extends Elfo {
    private final double QTD_VIDA_GANHA = 10;
    private int qtdAtaques;
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    {
        this.qtdAtaques = 0;
    }
    
    public ElfoDeLuz( String nome ) {
        super(nome);
        qtdDano = 21;
        super.ganharItem(new ItemSempreExistente( 1, DESCRICOES_VALIDAS.get(0) ));
    }
    
    @Override
    public void perderItem( Item item ) {
        if( !item.getDescricao().equals( DESCRICOES_VALIDAS.get(0) )){
            this.inventario.remover( item );
        }
    }
    
    private boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }
    
    private Item getEspada() {
        return this.getInventario().buscar( DESCRICOES_VALIDAS.get(0) );
    }
    
    private void ganharVida() {
        super.vida += QTD_VIDA_GANHA;
    }
    
    
    public void atacar(Anao anao){
        this.atacarComEspada(anao);
    }
    
    public void atacarComEspada( Anao anao ) {
        Item espada = getEspada();
        if( espada.getQuantidade() > 0 ) {
            qtdAtaques++;
            anao.sofrerDano();
            if( this.devePerderVida() ) {
                sofrerDano();
            } else {
                ganharVida();
            }
        }
    }
    
}