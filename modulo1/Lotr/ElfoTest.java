import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

<<<<<<< HEAD
public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }    
    
    @Test 
    public void criaDoisElfosVerificaQuantidadeElfos(){
         Elfo elfo = new Elfo( "Legolas" );
         Elfo elfo2 = new Elfo( "Legolas" );
         
         assertEquals( 2 , Elfo.getQtdElfos() );
    }
    
    @Test
    public void exemploMudancaValoresItem(){
        Elfo elfo = new Elfo( "Legolas" );
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade(1000);
        assertEquals(1000,flecha.getQuantidade());
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas(){
        Elfo elfo = new Elfo( "Legolas" );
        assertEquals(2, elfo.getFlecha().getQuantidade());
    }
    
    @Test 
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP(){
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf anao = new Dwarf( "Anaozin" );
        elfo.atirarFlecha(anao);
        assertEquals(1, elfo.getFlecha().getQuantidade()); 
        assertEquals(1, elfo.getExperiencia());         
    } 
    
    @Test
    public void elfoAtira3FlechasGanhaXPZeraFlechas(){
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf anao = new Dwarf( "Anaozin" );
        for(int i=0; i<3; i++){
            elfo.atirarFlecha(anao);
        }
        assertEquals(0, elfo.getFlecha().getQuantidade()); 
        assertEquals(2, elfo.getExperiencia());
    }  
    
=======
public class ElfoTest {
    
    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void exemploMudancaValoresItem() {
        Elfo elfo = new Elfo( "Legolas" );
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade(1000);
        assertEquals( 1000, flecha.getQuantidade() );
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas() {
        Elfo elfo = new Elfo( "Legolas" );
        assertEquals( 2, elfo.getFlecha().getQuantidade() );
    }
    
    @Test
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP() {
        Elfo elfo = new Elfo( "Legolas" );
        Anao anao = new Anao( "Gimli" );
        elfo.atirarFlecha(anao);
        assertEquals( 1, elfo.getFlecha().getQuantidade() );
        assertEquals( 1, elfo.getExperiencia() );
    }
    
    @Test
    public void elfoAtira3FlechasGanha2XP() {
       Elfo elfo = new Elfo( "Legolas" );
       Anao anao = new Anao( "Gimli" );
       for( int i = 0; i < 3; i++ ) {
           elfo.atirarFlecha(anao);
       }
       assertEquals( 2, elfo.getExperiencia() );
       assertEquals( 0, elfo.getFlecha().getQuantidade() );
    }
    
    @Test
    public void elfoAtirarFlecha12vezesEvidaDoAnaoFica0() {
       Elfo elfo = new Elfo( "Legolas" );
       elfo.getFlecha().setQuantidade(12);
       Anao anao = new Anao( "Gimli" );
       for( int i = 0; i < 12; i++ ) {
           elfo.atirarFlecha(anao);
       }
       assertEquals( 12, elfo.getExperiencia() );
       assertEquals( 0, elfo.getFlecha().getQuantidade() );
       assertEquals( 0.0, anao.getVida(), 0.001 );
    }
    
    @Test
    public void elfoAtirarFlechaMultiplas() {
       Elfo elfo = new Elfo( "Legolas" );
       elfo.getFlecha().setQuantidade(1000);
       for( int i = 0; i < 1000; i++ ) {
           elfo.atirarFlecha(new Anao( "Gimli" + i ));
       }
       assertEquals( 1000, elfo.getExperiencia() );
       assertEquals( 0, elfo.getFlecha().getQuantidade() );
    }
>>>>>>> otherrep/hipotese-alternativa
}