import java.util.*;

<<<<<<< HEAD
public class ElfoVerde extends Elfo{

    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
                "Espada de aço valiriano",
                "Arco de Vidro",
                "Flecha de Vidro"
        )
    );


    public ElfoVerde( String nome ){
        super( nome );
        super.qtdExperienciaPorAtaque = 2;
    }
    
    private boolean podeGanharPerderItem( String descricao ) {
        return this.DESCRICOES_VALIDAS.contains( descricao );
    }    
    
    @Override
    public void ganharItem( Item item ) {
        if ( this.podeGanharPerderItem( item.getDescricao()) ){
            super.ganharItem( item );
        }  
=======
public class ElfoVerde extends Elfo {

    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano",
            "Arco de vidro",
            "Flecha de vidro"
        )
    );
    
    public ElfoVerde( String nome ) {
        super(nome);
        super.qtdExperienciaPorAtaque = 2;
    }
    
    private boolean isDescricaoValida( String descricao ) {
        return DESCRICOES_VALIDAS.contains( descricao );
    }
    
    @Override
    public void ganharItem( Item item ) {
        if( this.isDescricaoValida( item.getDescricao() ) ){
            super.inventario.adicionar( item );
        }
>>>>>>> otherrep/hipotese-alternativa
    }
    
    @Override
    public void perderItem( Item item ) {
<<<<<<< HEAD
        if ( this.podeGanharPerderItem( item.getDescricao()) ) {
            super.perderItem( item );
        }    
    }
    
}
=======
        if( this.isDescricaoValida( item.getDescricao() ) ){
            super.inventario.remover( item );
        }
    }
}
>>>>>>> otherrep/hipotese-alternativa
