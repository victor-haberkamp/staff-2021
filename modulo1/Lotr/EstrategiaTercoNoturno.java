import java.util.*;
        
public class EstrategiaTercoNoturno implements EstrategiaAtk {
    private final int PROPORCAO_NOTURNOS = 3;
            
    public ArrayList<Elfo> getOrdemDeAtaque( ArrayList<Elfo> atacantes ){
        ArrayList<Elfo> elfosOrdenados = new ArrayList<>();
        int qtdNoturnos = 0, qtdVerdes = 0, qtdNoturnosPermitida, noturnosAMais;
        
        for( Elfo atacante : atacantes ){
            if ( temFlechasETaVivo( atacante ) ){
                elfosOrdenados.add(atacante);
                if( ehElfoNoturno( atacante ) ) { 
                    qtdNoturnos++; 
                } else {
                    qtdVerdes++;
                }
            }    
        }
        
        qtdNoturnosPermitida = (qtdVerdes++ / PROPORCAO_NOTURNOS );
        noturnosAMais = qtdNoturnos - qtdNoturnosPermitida;
        ordenarPorQtdFlecha( elfosOrdenados );
        
        if(noturnosAMais > 0 ) {
            retirarNoturnosSobrando( elfosOrdenados, noturnosAMais );
        }
        
        return elfosOrdenados;
    }
    
    private boolean temFlechasETaVivo( Elfo elfo ){
        return elfo.getStatus() != Status.MORTO && 
            (elfo.getFlecha().getQuantidade()) > 0;
    }    
    
    private boolean ehElfoNoturno( Elfo elfo ){
        return (elfo instanceof ElfoNoturno);
    }   
    
    private void ordenarPorQtdFlecha ( ArrayList<Elfo> elfos) {
        for( int i = 0; i < elfos.size(); i++ ) {
            for( int j = 0; j < elfos.size() - 1; j++ ) {
                int atual = this.getNumFlechas( elfos.get(j));
                int proximo = this.getNumFlechas( elfos.get(j+1));
                if( proximo > atual ) {
                    Elfo elfoTrocado = elfos.get( atual );
                    elfos.set(j, elfos.get( proximo ) );
                    elfos.set(j + 1, elfoTrocado);
                }
            }
        }
    }
    
    private int getNumFlechas( Elfo elfo){
        return elfo.getFlecha().getQuantidade();
    }    
    
    private void retirarNoturnosSobrando (ArrayList<Elfo> elfosOrdenados, int noturnosAMais ) {
        for ( int i = elfosOrdenados.size() - 1 ; noturnosAMais > 0 ; i-- ){
            if ( ehElfoNoturno ( elfosOrdenados.get(i) ) ) {
                elfosOrdenados.remove( i );
                noturnosAMais--;
            }    
        }
    }
}