import java.util.*;

public class ExercitoDeElfos {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    private ArrayList<EstrategiaAtk> estrategias = new ArrayList<>();
    
    
    public void alistar( Elfo elfo ) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains( elfo.getClass() );
        
        if( podeAlistar ) {
            elfos.add( elfo );
            
            ArrayList<Elfo> elfoDeUmStatus = porStatus.get( elfo.getStatus() );
            if( elfoDeUmStatus == null ) {
                elfoDeUmStatus = new ArrayList<>();
                porStatus.put( elfo.getStatus(), elfoDeUmStatus );
            }
            elfoDeUmStatus.add( elfo );
        }
    }
    
    public ArrayList<Elfo> buscar( Status status ) {
        return this.porStatus.get( status );
    }
    
    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
    
    public ArrayList<Elfo> montarEstrategia(EstrategiaAtk estrategia) {
        return estrategia.getOrdemDeAtaque(this.getElfos());
    }
    
    public void atacar( EstrategiaAtk estrategia , ArrayList<Anao> anoes){
        ArrayList<Elfo> atacantes = estrategia.getOrdemDeAtaque(this.getElfos());
        for(int i = 0; i < atacantes.size(); i++) {
            if(i < atacantes.size() && i < anoes.size() )
                atacantes.get(i).atacar(anoes.get(i));
        }
    }
}