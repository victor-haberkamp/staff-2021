<<<<<<< HEAD


=======
>>>>>>> otherrep/hipotese-alternativa
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

<<<<<<< HEAD
public class InventarioTest{
    @Test
    public void criaInventario(){
        Inventario inventario1 = new Inventario ();
        assertEquals(0, inventario1.getTamInventario()); 
    }
    
   
    @Test
    public void adicionaItensCorretamente(){
        Inventario inventario = new Inventario ();
        Item item1 = new Item(2, "Flecha");
        Item item2 = new Item(2, "Colar");
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        assertEquals(item1, inventario.getInventario().get(0));
        assertEquals(item2, inventario.getInventario().get(1));
    }
     
    @Test
    public void obtemItemCorretamente(){
        Inventario inventario = new Inventario ();
        Item item = new Item(2, "Flecha");
        inventario.adicionar(item);
        assertEquals(item, inventario.getInventario().get(0));
    }
    
    @Test
    public void removeItemDoInventario(){
        Inventario inventario = new Inventario ();
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(1, "Espada");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        
        inventario.remover(1);
        
        assertEquals(item1, inventario.obter(0));
        assertEquals(item3, inventario.obter(1));
        assertEquals(2, inventario.getInventario().size());
    }
    
    @Test
    public void testaGetDescricoesItems(){
        Inventario inventario = new Inventario ();
        String resultado = inventario.getDescricoesItens();
        assertEquals("", resultado);
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(1, "Espada");
        Item item5 = new Item(1, "Bracelete");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        resultado = inventario.getDescricoesItens();
        assertEquals("Flecha,Arco,Espada", resultado);
     
        inventario.adicionar(item5);
        resultado = inventario.getDescricoesItens();
        assertEquals("Flecha,Arco,Espada,Bracelete", resultado);   
        
        inventario.remover(0);
        resultado = inventario.getDescricoesItens();
        assertEquals("Arco,Espada,Bracelete", resultado);
    }

    @Test
    public void inventarioRetornaItemMaiorQuantidade(){
        Inventario inventario = new Inventario ();
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(1, "Espada");
        Item item4 = new Item(7, "Bracelete");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        assertEquals(item1, inventario.getItemMaiorQuantidade());
    
        inventario.remover(0);
        assertEquals(item2, inventario.getItemMaiorQuantidade());

        inventario.adicionar(item4);
        assertEquals(item4, inventario.getItemMaiorQuantidade());   
        
    } 
    
    @Test
    public void buscaItemRetornaItem(){
        Inventario inventario = new Inventario ();
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(1, "Espada");
        Item item4 = new Item(7, "Bracelete");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        
        assertEquals(item1 , inventario.buscar("Flecha"));
        assertEquals(item2 , inventario.buscar("Arco"));
        assertEquals(item3 , inventario.buscar("Espada"));
        assertEquals(item4 , inventario.buscar("Bracelete"));
    }   
    
    @Test
    public void inverteItensInvertarioNaoAlteraOriginal(){
        Inventario inventario = new Inventario ();
        
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(1, "Espada");
        Item item4 = new Item(7, "Bracelete");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        

        ArrayList<Item> inventarioInvertido = inventario.inverter();
        
        assertEquals(inventarioInvertido.get(0), item4);
        assertEquals(inventarioInvertido.get(3), item1);
        
        assertEquals(inventario.obter(0), item1);
        assertEquals(inventario.obter(3), item4);
        
    }
    
    @Test
    public void ordenaInventario(){
        Inventario inventario = new Inventario ();
        
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(7, "Bracelete");
        Item item4 = new Item(1, "Espada");

        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        
        inventario.ordenarItens();
        
        assertEquals("Arco,Espada,Flecha,Bracelete", inventario.getDescricoesItens());
    }    
    
    @Test
    public void ordenaInventarioRecebendoParametro(){
        Inventario inventario = new Inventario ();
        
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(7, "Bracelete");
        Item item4 = new Item(1, "Espada");

        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        
        inventario.ordenarItens(TipoOrdenacao.DESC);
        
        assertEquals("Bracelete,Flecha,Arco,Espada", inventario.getDescricoesItens());
    }
}
=======
public class InventarioTest {
    
    /*@Test
    public void criarInventarioSemPassarValor(){
        Inventario inventario = new Inventario();
        assertEquals( 99, inventario.getItens().size() );
    }
    
    @Test
    public void criarInventarioPassandoValor(){
        Inventario inventario = new Inventario(40);
        assertEquals( 40, inventario.getItens().length );
    }*/
    
    @Test
    public void adicionarUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        inventario.adicionar(item);
        assertEquals( item, inventario.getItens().get(0) );
    }
    
    @Test
    public void adicionarDoisItensInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        Item item2 = new Item( 3, "Flecha de Luz" );
        inventario.adicionar(item);
        inventario.adicionar(item2);
        assertEquals( item, inventario.getItens().get(0) );
        assertEquals( item2, inventario.getItens().get(1) );
    }
    
    @Test
    public void obterUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        inventario.adicionar(item);
        assertEquals( item, inventario.obter(0) );
    }
    
    @Test
    public void removerUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item( 1, "Espada de aço" );
        inventario.adicionar(item);
        inventario.remover(0);
        assertNull( inventario.obter(0) );
    }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item escudo = new Item( 2, "Escudo" );
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals( "Espada,Escudo", resultado );
    }
    
    @Test
    public void getItemMaiorQuantidade() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item lanca = new Item( 5, "Lança" );
        Item escudo = new Item( 2, "Escudo" );
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        Item maiorQtd = inventario.getItemComMaiorQuantidade();
        assertEquals( lanca, maiorQtd );
    }
    
    @Test
    public void getItemMaiorQuantidadeComItensDeMesmaQuantidade() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item lanca = new Item( 5, "Lança" );
        Item escudo = new Item( 5, "Escudo" );
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        Item maiorQtd = inventario.getItemComMaiorQuantidade();
        assertEquals( lanca, maiorQtd );
    }
    
    @Test
    public void buscarApenasUmItem() {
        Inventario inventario = new Inventario();
        Item cafe = new Item( 1, "Cafe" );
        inventario.adicionar(cafe);
        Item resultado = inventario.buscar(new String( "Cafe" ));
        assertEquals( cafe, resultado );
    }
    
    @Test
    public void inverterDoisItens() {
        Inventario inventario = new Inventario();
        Item termica = new Item( 1, "Termica de Café" );
        Item energetico = new Item( 1, "Energetico" );
        inventario.adicionar( termica );
        inventario.adicionar( energetico );
        ArrayList<Item> resultado = inventario.inverter();
        
        assertEquals( energetico, resultado.get(0) );
        assertEquals( termica, resultado.get(1) );
        
        assertEquals( termica, inventario.obter(0) );
        assertEquals( energetico, inventario.obter(1) );
        
        assertEquals( 2, resultado.size() );
    }
    
    @Test
    public void ordenarComApenasUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarTotalmenteDesordenado() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarTotalmenteOrdenado() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarParcialmenteOrdenado() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarQuantidadesIguais() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(escudo, cafe, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }
     
    @Test
    public void ordernarDescComApenasUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescTotalmenteOrdenado() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescTotalmenteDesordenado() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item cafe = new Item(3, "Térmica de café");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescParcialmenteDesordenado() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }    

    @Test
    public void ordernarDescQuantidadesIguais() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(escudo, cafe, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
}
>>>>>>> otherrep/hipotese-alternativa
