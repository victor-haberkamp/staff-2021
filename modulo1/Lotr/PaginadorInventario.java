import java.util.*;

<<<<<<< HEAD
public class PaginadorInventario{
    private Inventario inventario;
    private int marcadorInicial = 0;
    
    public PaginadorInventario( Inventario inventario){
        this.inventario = inventario;
    }
    
    public void pular(int marcadorInicial){
        this.marcadorInicial = marcadorInicial > 0 ? marcadorInicial : 0;
    }
    
    public ArrayList<Item> limitar (int numItens){
        ArrayList<Item> pagina = new ArrayList<>();
        int fim = numItens + marcadorInicial;
        for( int i = this.marcadorInicial; i<fim &&  i < this.inventario.getInventario().size() ; i++){
            pagina.add(inventario.obter( i ));
        }
        return pagina;
    }
}
=======
public class PaginadorInventario {
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario( Inventario inventario ) {
        this.inventario = inventario;
    }
    
    public void pular( int marcador ) {
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar( int qtd ) {
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + qtd;
        for( int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++ ) {
            subConjunto.add( this.inventario.obter(i) );
        }
        
        return subConjunto;
    }
}
>>>>>>> otherrep/hipotese-alternativa
