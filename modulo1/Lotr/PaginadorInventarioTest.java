import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

<<<<<<< HEAD
public class PaginadorInventarioTest{
    @Test
    public void testaPular(){
        Inventario inventario = new Inventario ();
        
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(7, "Bracelete");
        Item item4 = new Item(1, "Espada");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);

        PaginadorInventario paginador = new PaginadorInventario( inventario );
        
        paginador.pular( 1 );
        ArrayList<Item> pagina = paginador.limitar(1);
        
        assertEquals(item2, pagina.get(0));
    }
    
        @Test
    public void testaLimitarDentroDosLimites(){
        Inventario inventario = new Inventario ();
        
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(7, "Bracelete");
        Item item4 = new Item(1, "Espada");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        
        paginador.pular( 2 );
        ArrayList<Item> pagina = paginador.limitar(2);
        
        assertEquals(item3, pagina.get(0));
        assertEquals(item4, pagina.get(1));
    }
    
    @Test
    public void testaLimitarForaDosLimites(){
        Inventario inventario = new Inventario ();
        
        Item item1 = new Item(3, "Flecha");
        Item item2 = new Item(1, "Arco");
        Item item3 = new Item(7, "Bracelete");
        Item item4 = new Item(1, "Espada");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);
        
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        
        paginador.pular( 1 );
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        paginador.pular( 3 );
        ArrayList<Item> segundaPagina = paginador.limitar(2);
        
        
        assertEquals(item2, primeiraPagina.get(0));
        assertEquals(item3, primeiraPagina.get(1));
        assertEquals( 1, segundaPagina.size());
        assertEquals(item4, segundaPagina.get(0));
    }
    
}
=======
public class PaginadorInventarioTest {
    
    @Test
    public void pularLimitarComApenasUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        inventario.adicionar( espada );
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals( espada, primeiraPagina.get(0) );
        assertEquals( 1, primeiraPagina.size() );
    }
    
    @Test
    public void pularLimitarDentroDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item escudo = new Item( 2, "Escudo" );
        Item lanca = new Item( 10, "Lança" );
        inventario.adicionar( espada );
        inventario.adicionar( escudo );
        inventario.adicionar( lanca );
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1);
        
        assertEquals( espada, primeiraPagina.get(0) );
        assertEquals( escudo, primeiraPagina.get(1) );
        assertEquals( 2, primeiraPagina.size() );
        
        assertEquals( lanca, segundaPagina.get(0) );
        assertEquals( 1, segundaPagina.size() );
    }
    
    @Test
    public void pularLimitarForaDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        Item escudo = new Item( 2, "Escudo" );
        Item lanca = new Item( 10, "Lança" );
        inventario.adicionar( espada );
        inventario.adicionar( escudo );
        inventario.adicionar( lanca );
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1000);
        
        assertEquals( espada, primeiraPagina.get(0) );
        assertEquals( escudo, primeiraPagina.get(1) );
        assertEquals( 2, primeiraPagina.size() );
        
        assertEquals( lanca, segundaPagina.get(0) );
        assertEquals( 1, segundaPagina.size() );
    }
}
>>>>>>> otherrep/hipotese-alternativa
