package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.DTO.AcessoDTOResponse;
import br.com.dbccompany.coworking.DTO.AcessoDTOSaldo;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/acesso" )
public class AcessoController {

    @Autowired
    private AcessoService service;

    @PostMapping(value = "/registrarAcesso")
    @ResponseBody
    public ResponseEntity<AcessoDTOSaldo> registrarAcesso(@RequestBody AcessoDTO element) {
        try {
            return new ResponseEntity<>( service.registraAcesso(element), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<AcessoDTOResponse> trazerElementoEspecifico(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<AcessoDTOResponse>> trazerTodos() {
        try {
            return new ResponseEntity<>(service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<AcessoDTOResponse> salvarEspacoPacote(@RequestBody AcessoDTO element) {
        try {
            return new ResponseEntity<>( service.save(element), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

}
