package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.Cliente_PacoteDTO;
import br.com.dbccompany.coworking.DTO.Cliente_PacoteDTOResponse;
import br.com.dbccompany.coworking.Service.Cliente_PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/clientePacote" )
public class Cliente_PacoteController {

    @Autowired
    private Cliente_PacoteService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Cliente_PacoteDTOResponse> trazerElementoEspecifico(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<Cliente_PacoteDTOResponse>> trazerTodosElementos() {
        try {
            return new ResponseEntity<>(service.trazerTodosOsClientePacote(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Cliente_PacoteDTOResponse> salvarEspacoPacote(@RequestBody Cliente_PacoteDTO element) {
        try {
            return new ResponseEntity<>( service.save(element), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Cliente_PacoteDTOResponse> editarItem(@RequestBody Cliente_PacoteDTO pacote, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.edit(pacote, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
