package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/cliente")
public class ClientesController {


    @Autowired
    private ClientesService service;

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<ClienteDTO> trazerClienteEspecifico(@PathVariable Integer id ){
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<ClienteDTO>> trazerTodosClientes(){
        try {
            return new ResponseEntity<>(service.trazerTodosOsClientes(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<ClienteDTO> salvarCliente(@RequestBody ClienteDTO clientes ){
        try {
            return new ResponseEntity<>( service.save(clientes), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping ( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ClienteDTO> editarClientes(@RequestBody ClienteDTO clientes, @PathVariable Integer id){
        try {
            return new ResponseEntity<>( service.edit(clientes, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
