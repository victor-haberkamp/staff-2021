package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contratacao" )
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<ContratacaoDTOResponse> trazerElementoEspecifico(@PathVariable Integer id ){
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTOResponse>> trazerTodosItens() {
        try {
            return new ResponseEntity<>(service.trazerTodosOsItens(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContratacaoDTOResponse> salvarItem(@RequestBody ContratacaoDTO contato ){
        try {
            return new ResponseEntity<>( service.save(contato), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

}
