package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.Espaco_PacoteDTO;
import br.com.dbccompany.coworking.Service.Espaco_PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espacoPacote")
public class Espaco_PacoteController {

    @Autowired
    private Espaco_PacoteService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Espaco_PacoteDTO> trazerElementoEspecifico(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<Espaco_PacoteDTO>> trazerTodosElementos() {
        try {
            return new ResponseEntity<>(service.trazerTodosEspacoPacote(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Espaco_PacoteDTO> salvarEspacoPacote(@RequestBody Espaco_PacoteDTO element) {
        try {
            return new ResponseEntity<>( service.save(element), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Espaco_PacoteDTO> editarItem(@RequestBody Espaco_PacoteDTO pacote, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.edit(pacote, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
