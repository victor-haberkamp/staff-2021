package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espacos")
public class EspacosController {

    @Autowired
    private EspacosService service;

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<EspacoDTO> trazerEspacoEspecifico(@PathVariable Integer id ){
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<EspacoDTO>> trazerTodosItens(){
        try {
            return new ResponseEntity<>(service.trazerTodosEspacos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<EspacoDTO> salvarItem(@RequestBody EspacoDTO contato ){
        try {
            return new ResponseEntity<>( service.save(contato), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping ( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> editarItem(@RequestBody EspacoDTO espaco, @PathVariable Integer id){
        try {
            return new ResponseEntity<>( service.edit(espaco, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
