package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTOResponse;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/saldoCliente" )
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService service;

    @GetMapping(value = "/buscarPorId")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTOResponse> trazerElementoEspecifico(@RequestBody SaldoClienteId id) {
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<SaldoClienteDTOResponse>> trazerTodosElementos() {
        try {
            return new ResponseEntity<>(service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTOResponse> salvarItem(@RequestBody SaldoClienteDTO saldoCliente) {
        try {
            return new ResponseEntity<>( service.save( saldoCliente ), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

}
