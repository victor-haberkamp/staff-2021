package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.DTO.Tipo_ContatoDTO;
import br.com.dbccompany.coworking.Service.Tipo_ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value="/api/tipoContato" )
public class Tipo_ContatoController {

    @Autowired
    private Tipo_ContatoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<Tipo_ContatoDTO>> trazerTodosItens(){
        try {
            return new ResponseEntity<>(service.trazerTodosOsItens(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<Tipo_ContatoDTO> trazerItemEspecifico( @PathVariable Integer id ){
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Tipo_ContatoDTO> salvarTipoContato(@RequestBody Tipo_ContatoDTO tipoContato ){
        try {
            return new ResponseEntity<>( service.save(tipoContato), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping ( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Tipo_ContatoDTO> editarItem( @RequestBody Tipo_ContatoDTO tipoContato, @PathVariable Integer id){
        try {
            return new ResponseEntity<>( service.edit(tipoContato, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}