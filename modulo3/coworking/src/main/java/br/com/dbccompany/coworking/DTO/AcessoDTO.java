package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessoEntity;

import java.time.LocalDateTime;

public class AcessoDTO {
    private Integer clienteId;
    private Integer espacoId;
    private Boolean isEntrada;
    private Boolean especial = false;
    private LocalDateTime data;

    public AcessoEntity convert(){
        AcessoEntity acesso = new AcessoEntity();
        acesso.setEntrada( this.isEntrada );
        acesso.setEspecial( this.especial );
        acesso.setData( this.data == null ? LocalDateTime.now() : this.data );
        return acesso;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer esppacoId) {
        this.espacoId = esppacoId;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Boolean getEspecial() {
        return especial;
    }

    public void setEspecial(Boolean especial) {
        this.especial = especial;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }
}
