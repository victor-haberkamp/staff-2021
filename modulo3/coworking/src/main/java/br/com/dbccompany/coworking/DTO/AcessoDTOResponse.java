package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessoEntity;

import java.time.LocalDateTime;

public class AcessoDTOResponse {
    private ClienteDTO cliente;
    private EspacoDTO espaco;
    private Boolean isEntrada;
    private Boolean especial;
    private LocalDateTime data;

    public AcessoDTOResponse(){ }

    public AcessoDTOResponse(AcessoEntity acesso){
        this.cliente = new ClienteDTO( acesso.getSaldoCliente().getCliente() );
        this.espaco = new EspacoDTO( acesso.getSaldoCliente().getEspaco() );
        this.isEntrada = acesso.getEntrada();
        this.especial = acesso.getEspecial();
        this.data = acesso.getData();
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Boolean getEspecial() {
        return especial;
    }

    public void setEspecial(Boolean especial) {
        this.especial = especial;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }
}
