package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessoEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class AcessoDTOSaldo {
    private String saldo;

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}
