package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ClienteDTO {
    private Integer id;
    private String nome;
    private String cpf;
    private LocalDate dataDeNascimento;
    private ArrayList<ContatoDTO> contato = new ArrayList<ContatoDTO>();

    public ClienteDTO(){}

    public ClienteDTO(ClienteEntity entity){
        this.id = entity.getId();
        this.nome = entity.getNome();
        this.cpf = entity.getCpf();
        this.dataDeNascimento = entity.getDataDeNascimento();
        this.contato = this.convertEntityToDTO( entity.getContato() );

    }

    public ClienteEntity convert(){
        ClienteEntity entity = new ClienteEntity();
        entity.setId( this.id );
        entity.setNome( this.nome );
        entity.setCpf( this.cpf );
        entity.setDataDeNascimento( this.dataDeNascimento );
        entity.setContato( this.convertDTOToEntity( this.contato ) );
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(LocalDate dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public ArrayList<ContatoDTO> getContato() {
        return contato;
    }

    public void setContato(ArrayList<ContatoDTO> contato) {
        this.contato = contato;
    }

    private ArrayList<ContatoDTO> convertEntityToDTO(List<ContatoEntity> entidades ){
        ArrayList<ContatoDTO> contatosConvertidos = new ArrayList<ContatoDTO>();
        for( ContatoEntity entidade:entidades){
            contatosConvertidos.add( new ContatoDTO( entidade ) );
        }
        return contatosConvertidos;
    }

    private List<ContatoEntity> convertDTOToEntity(List<ContatoDTO> dtos){
        ArrayList<ContatoEntity> contatosConvertidos = new ArrayList<ContatoEntity>();
        for( ContatoDTO dto:dtos){
            contatosConvertidos.add( dto.convert() );
        }
        return contatosConvertidos;

    }

}
