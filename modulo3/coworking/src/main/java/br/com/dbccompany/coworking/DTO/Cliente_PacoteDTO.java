package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;

public class Cliente_PacoteDTO {
    private Integer id;
    private PacoteDTO pacote;
    private ClienteDTO cliente;
    private Integer quantidade;

    public Cliente_PacoteDTO(){}

    public Cliente_PacoteDTO(Cliente_PacoteEntity entity){
        this.id = entity.getId();
        this.pacote = new PacoteDTO( entity.getPacote() );
        this.cliente = new ClienteDTO( entity.getCliente() );
        this.quantidade = entity.getQuantidade();
    }

    public Cliente_PacoteEntity convert (){
        Cliente_PacoteEntity entity = new Cliente_PacoteEntity();
        entity.setId( this.id );
        entity.setQuantidade( this.quantidade );
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}