package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;

public class ContatoDTO {
    private Integer id;
    private String valor;
    private Tipo_ContatoDTO tipoContato;

    public ContatoDTO() {}

    public ContatoDTO(ContatoEntity entidade){
        this.id = entidade.getId();
        this.valor = entidade.getValor();
        this.tipoContato = new Tipo_ContatoDTO( entidade.getTipoContato() );
    }

    public ContatoEntity convert (){
        ContatoEntity entity = new ContatoEntity();
        entity.setId( this.id );
        entity.setValor( this.valor );
        if( this.tipoContato != null ) {
            entity.setTipoContato(tipoContato.convert());
        }
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Tipo_ContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(Tipo_ContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }
}
