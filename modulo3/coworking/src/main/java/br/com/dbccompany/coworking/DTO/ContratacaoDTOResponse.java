package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;

public class ContratacaoDTOResponse {
    private Integer id;
    private Double valorASerCobrado;
    private Integer prazo;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private EspacoDTO espaco;
    private ClienteDTO cliente;

    public ContratacaoDTOResponse(){}

    public ContratacaoDTOResponse( ContratacaoEntity entity ){
        this.id = entity.getId();
        this.valorASerCobrado = entity.retornaValorDeContratacao();
        this.prazo = entity.getPrazo();
        this.tipoContratacao = entity.getTipoContratacao();
        this.quantidade = entity.getQuantidade();
        this.desconto = entity.getDesconto();
        this.espaco = new EspacoDTO( entity.getEspaco() );
        this.cliente = new ClienteDTO( entity.getCliente() );
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValorASerCobrado() {
        return valorASerCobrado;
    }

    public void setValorASerCobrado(Double valorASerCobrado) {
        this.valorASerCobrado = valorASerCobrado;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }
}
