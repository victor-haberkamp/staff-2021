package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoEntity;

public class EspacoDTO {
    private Integer id;
    private String nome;
    private Integer qtdPessoas;
    private String valor;

    public EspacoDTO(){}

    public EspacoDTO(EspacoEntity entity){
        this.id = entity.getId();
        this.nome = entity.getNome();
        this.qtdPessoas = entity.getQtdPessoas();
        this.valor = "R$"+entity.getValor();
    }

    public EspacoEntity convert(){
        EspacoEntity entity = new EspacoEntity();
        entity.setId( this.id );
        entity.setNome( this.nome );
        entity.setQtdPessoas( this.qtdPessoas );
        entity.setValor( Double.parseDouble(this.valor.substring(2)) );
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
