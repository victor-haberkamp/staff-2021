package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;

public class Espaco_PacoteDTO {
    private Integer id;
    private PacoteDTO pacote;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;
    private EspacoDTO espaco;

    public Espaco_PacoteDTO(){}

    public Espaco_PacoteDTO(Espaco_PacoteEntity entity){
        this.id = entity.getId();
        this.pacote = new PacoteDTO(entity.getPacote());
        this.tipoContratacao = entity.getTipoContratacao();
        this.quantidade = entity.getQuantidade();
        this.prazo = entity.getPrazo();
        this.espaco = new EspacoDTO( entity.getEspaco() );
    }

    public Espaco_PacoteEntity convert(){
        Espaco_PacoteEntity entity = new Espaco_PacoteEntity();
        entity.setId( this.id );
        entity.setTipoContratacao( tipoContratacao );
        entity.setQuantidade( this.quantidade );
        entity.setPrazo( this.prazo );
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantide) {
        this.quantidade = quantide;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }
}
