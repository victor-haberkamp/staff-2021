package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;

public class Espaco_PacoteDTORetorno {
    private Integer id;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;
    private EspacoDTO espaco;

    public Espaco_PacoteDTORetorno(){}

    public Espaco_PacoteDTORetorno(Espaco_PacoteEntity entity) {
        this.id = entity.getId();
        this.tipoContratacao = entity.getTipoContratacao();
        this.quantidade = entity.getQuantidade();
        this.prazo = entity.getPrazo();
        this.espaco = new EspacoDTO(entity.getEspaco());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantide) {
        this.quantidade = quantide;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

}
