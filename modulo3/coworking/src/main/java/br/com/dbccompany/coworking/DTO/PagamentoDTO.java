package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_PagamentoEnum;

import java.util.List;

public class PagamentoDTO {
    private Integer id;
    private Integer clientePacoteId;
    private Integer contratacaoId;
    private Tipo_PagamentoEnum tipoPagamento;

    public PagamentoDTO(){}

    public PagamentoEntity convert(){
        PagamentoEntity entity = new PagamentoEntity();
        entity.setId( this.id );
        entity.setTipoPagamento( this.tipoPagamento );
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientePacoteId() {
        return clientePacoteId;
    }

    public void setClientePacoteId(Integer clientePacoteId) {
        this.clientePacoteId = clientePacoteId;
    }

    public Integer getContratacaoId() {
        return contratacaoId;
    }

    public void setContratacaoId(Integer contratacaoId) {
        this.contratacaoId = contratacaoId;
    }

    public Tipo_PagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(Tipo_PagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
