package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_PagamentoEnum;

public class PagamentoDTOResponse {
    private Integer id;
    private Tipo_PagamentoEnum tipoPagamento;

    public PagamentoDTOResponse(){}

    public PagamentoDTOResponse(PagamentoEntity entity){
        this.id = entity.getId();
        this.tipoPagamento = entity.getTipoPagamento();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tipo_PagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(Tipo_PagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
