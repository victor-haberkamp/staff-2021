package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;

public class PagamentoDTOResponsePacote extends PagamentoDTOResponse {
    private Cliente_PacoteDTOResponse clientePacote;

    public PagamentoDTOResponsePacote(PagamentoEntity entity){
        super( entity );
        this.clientePacote = new Cliente_PacoteDTOResponse( entity.getClientePacote() );
    }

    public Cliente_PacoteDTOResponse getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Cliente_PacoteDTOResponse clientePacote) {
        this.clientePacote = clientePacote;
    }
}
