package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;

import java.time.LocalDate;

public class SaldoClienteDTO {
    private Integer id_cliente;
    private Integer id_espaco;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;

    public SaldoClienteDTO(){}

    public SaldoClienteEntity convert(){
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId( new SaldoClienteId( this.id_cliente, this.id_espaco ) );
        saldoCliente.setTipoContratacao( tipoContratacao );
        saldoCliente.setQuantidade( this.quantidade );
        saldoCliente.setVencimento( this.vencimento );
        return saldoCliente;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_espaco() {
        return id_espaco;
    }

    public void setId_espaco(Integer id_espaco) {
        this.id_espaco = id_espaco;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }
}
