package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;

import java.time.LocalDate;

public class SaldoClienteDTOResponse {
    private ClienteDTO cliente;
    private EspacoDTO espaco;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;

    public SaldoClienteDTOResponse(){}

    public SaldoClienteDTOResponse(SaldoClienteEntity entity){
        this.cliente = new ClienteDTO( entity.getCliente() );
        this.espaco = new EspacoDTO(entity.getEspaco() );
        this.tipoContratacao = entity.getTipoContratacao();
        this.quantidade = entity.getQuantidade();
        this.vencimento = entity.getVencimento();
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }
}
