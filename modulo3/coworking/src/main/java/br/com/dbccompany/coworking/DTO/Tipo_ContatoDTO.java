package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;

public class Tipo_ContatoDTO {
    private Integer id;
    private String nome;

    public Tipo_ContatoDTO(){}

    public Tipo_ContatoDTO(Tipo_ContatoEntity entity){
        this.id = entity.getId();
        this.nome = entity.getNome();
    }

    public Tipo_ContatoEntity convert (){
        Tipo_ContatoEntity entity = new Tipo_ContatoEntity();
        entity.setId( this.id );
        entity.setNome( nome );
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
