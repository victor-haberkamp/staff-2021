package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;

public class UsuarioDTOResponse {
    private String nome;
    private String email;
    private String login;
    private String senha;

    public UsuarioDTOResponse(UsuarioEntity user){
        this.nome = user.getNome();
        this.email = user.getEmail();
        this.login = user.getUsername();
        this.senha = user.getPassword();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
