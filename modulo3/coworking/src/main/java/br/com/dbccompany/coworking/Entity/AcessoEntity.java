package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity {

    @Id
    @SequenceGenerator( name = "ACESSO_PACOTE_SEQ", sequenceName = "ACESSO_PACOTE_SEQ")
    @GeneratedValue( generator= "ACESSO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    private SaldoClienteEntity saldoCliente;

    private Boolean isEntrada = false;

    @Column(nullable = false)
    private LocalDateTime data;

    private Boolean especial = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getEspecial() {
        return especial;
    }

    public void setEspecial(Boolean especial) {
        this.especial = especial;
    }
}
