package br.com.dbccompany.coworking.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class ClienteEntity {

    @Id
    @SequenceGenerator( name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue( generator= "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true )
    private Character[] cpf = new Character[11];

    @Column( nullable = false)
    @DateTimeFormat( pattern = "dd/MM/yyyy" )
    private LocalDate dataDeNascimento;

    @OneToMany( mappedBy = "cliente",
            orphanRemoval = true)
    private List<ContatoEntity> contato = new ArrayList<ContatoEntity>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        String cpfRetorno = new String()        ;
        for( int i=0; i< 11; i++){
            if( i == 3 || i == 6){
                cpfRetorno += ".";
            }else if( i == 9){
                cpfRetorno +="-";
            }
            cpfRetorno += cpf[i];
        }
        return cpfRetorno;
    }

    public void setCpf(String cpf) {
        char[] cpfArray = cpf.toCharArray();
        for (int i=0, j=0; i<14; i++, j++) {
            if (i == 3 || i == 7 || i == 11) {
                i++;
            }
            this.cpf[j] = cpfArray[i];
        }
    }

    public LocalDate getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(LocalDate dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }
}
