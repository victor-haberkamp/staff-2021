package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class ContatoEntity {

    @Id
    @SequenceGenerator( name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue( generator= "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "tipo_contato_id" )
    private Tipo_ContatoEntity tipoContato;

    @Column( nullable = false )
    private String valor;

    @ManyToOne( fetch = FetchType.LAZY)
    private ClienteEntity cliente;

    public Tipo_ContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(Tipo_ContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
