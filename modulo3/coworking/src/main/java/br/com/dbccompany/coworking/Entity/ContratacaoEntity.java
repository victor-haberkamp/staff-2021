package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Util.Conversor;

import javax.persistence.*;

@Entity
public class ContratacaoEntity {

    @Id
    @SequenceGenerator( name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator= "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "espaco_id" )
    private EspacoEntity espaco;

    @ManyToOne
    @JoinColumn( name = "cliente_id" )
    private ClienteEntity cliente;

    @Column( nullable = false )
    private Tipo_ContratacaoEnum tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    private Double desconto;

    @Column( nullable = false )
    private Integer prazo;

    public Double retornaValorDeContratacao(){
        Double valorEspaco = this.espaco.getValor();
        Integer tempoMinutos = Conversor.convertToMinute( this.quantidade, tipoContratacao );
        if( desconto == null ){
            desconto = 0.0;
        }
        return (valorEspaco*tempoMinutos)*((100-this.desconto)/100);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
