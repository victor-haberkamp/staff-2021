package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity {

    @Id
    @SequenceGenerator( name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue( generator= "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( nullable = false )
    private Double valor;

    @OneToMany(mappedBy = "pacote")
    private List<Espaco_PacoteEntity> espacoPacote;

    @OneToMany(mappedBy = "pacote")
    private List<Cliente_PacoteEntity> clientePacote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Espaco_PacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<Espaco_PacoteEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<Cliente_PacoteEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<Cliente_PacoteEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }
}
