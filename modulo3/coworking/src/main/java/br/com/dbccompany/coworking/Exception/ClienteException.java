package br.com.dbccompany.coworking.Exception;

public class ClienteException extends Exception {
    private String message;

    public ClienteException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
