package br.com.dbccompany.coworking.Exception;

public class ErroAoRealizarOperacao extends Exception {
    private String message;

    public ErroAoRealizarOperacao(String message) {
        super(message);
    }

    public ErroAoRealizarOperacao() {
        super();
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}