package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Cliente_PacoteRepository extends CrudRepository<Cliente_PacoteEntity,Integer > {

    @Override
    Optional<Cliente_PacoteEntity> findById(Integer integer);
}
