package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {

    @Override
    Optional<ContratacaoEntity> findById(Integer integer);

}
