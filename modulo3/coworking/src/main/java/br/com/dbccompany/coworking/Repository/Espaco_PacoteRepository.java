package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Espaco_PacoteRepository extends CrudRepository<Espaco_PacoteEntity, Integer> {

    @Override
    Optional<Espaco_PacoteEntity> findById(Integer integer);
}
