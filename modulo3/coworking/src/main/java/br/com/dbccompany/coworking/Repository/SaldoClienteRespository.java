package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SaldoClienteRespository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
    @Override
    Optional<SaldoClienteEntity> findById(SaldoClienteId id);
}
