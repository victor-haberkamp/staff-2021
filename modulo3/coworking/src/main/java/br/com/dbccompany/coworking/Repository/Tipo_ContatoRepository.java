package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Tipo_ContatoRepository extends CrudRepository<Tipo_ContatoEntity, Integer> {
    @Override
    Optional<Tipo_ContatoEntity> findById(Integer integer);
    Tipo_ContatoEntity findByNome ( String nome );

}
