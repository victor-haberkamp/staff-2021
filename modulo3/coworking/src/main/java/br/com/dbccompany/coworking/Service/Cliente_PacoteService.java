package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.Cliente_PacoteDTO;
import br.com.dbccompany.coworking.DTO.Cliente_PacoteDTOResponse;
import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.Cliente_PacoteRepository;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Cliente_PacoteService {

    @Autowired
    private Cliente_PacoteRepository repository;

    @Autowired
    private ClientesRepository clienteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    public List<Cliente_PacoteDTOResponse> trazerTodosOsClientePacote() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Cliente_PacoteDTOResponse save( Cliente_PacoteDTO clientePacoteDTO ) throws ErroAoSalvar {
        try {
            Cliente_PacoteEntity clientePacote = clientePacoteDTO.convert();
            clientePacote.setCliente( clienteRepository.findById( clientePacoteDTO.getCliente().getId() ).get() );
            clientePacote.setPacote( pacoteRepository.findById( clientePacoteDTO.getPacote().getId() ).get() );
            return this.saveAndEdit( clientePacote );
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Cliente_PacoteDTOResponse edit(Cliente_PacoteDTO cliente, Integer id ) throws ErroAoRealizarOperacao {
        try{
            Cliente_PacoteEntity clienteEntity = cliente.convert();
            clienteEntity.setId(id);
            return this.saveAndEdit( clienteEntity );
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private Cliente_PacoteDTOResponse saveAndEdit(Cliente_PacoteEntity entity ){
        Cliente_PacoteEntity elementNovo = repository.save( entity );
        return new Cliente_PacoteDTOResponse( elementNovo );
    }

    public Cliente_PacoteDTOResponse findById(Integer id) throws RetornoDeBuscaInvalido {
        try{
            Optional<Cliente_PacoteEntity> element = repository.findById(id);
            return new Cliente_PacoteDTOResponse( repository.findById(id).get() );
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<Cliente_PacoteDTOResponse> convertList(Iterable<Cliente_PacoteEntity> entidades ){
        ArrayList<Cliente_PacoteDTOResponse> elementosConvertidos = new ArrayList<Cliente_PacoteDTOResponse>();
        for( Cliente_PacoteEntity entidade:entidades){
            elementosConvertidos.add( new Cliente_PacoteDTOResponse( entidade ) );
        }
        return elementosConvertidos;
    }

}