package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    public List<ContatoDTO> trazerTodosOsItens() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ContatoDTO save( ContatoDTO contato ) throws ErroAoSalvar {
        try {
            return this.saveAndEdit(contato.convert());
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ContatoDTO edit( ContatoDTO contato, Integer id ) throws ErroAoRealizarOperacao {
        try {
            ContatoEntity contatoEntity = contato.convert();
            contatoEntity.setId(id);
            return this.saveAndEdit(contatoEntity);
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private ContatoDTO saveAndEdit( ContatoEntity tipoContato ){
        ContatoEntity tipoNovo = repository.save( tipoContato );
        return new ContatoDTO( tipoNovo );
    }

    public ContatoDTO findById( Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<ContatoEntity> contato = repository.findById(id);
            return new ContatoDTO(repository.findById(id).get());
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }

    }

    private List<ContatoDTO> convertList( Iterable<ContatoEntity> entidades ){
        ArrayList<ContatoDTO> contatosConvertidos = new ArrayList<ContatoDTO>();
        for( ContatoEntity entidade:entidades){
            contatosConvertidos.add( new ContatoDTO( entidade ) );
        }
        return contatosConvertidos;
    }

}
