package br.com.dbccompany.coworking.Service;


import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.DTO.Tipo_ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.Tipo_ContatoRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Tipo_ContatoService {

    @Autowired
    private Tipo_ContatoRepository repository;

    public List<Tipo_ContatoDTO> trazerTodosOsItens() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Tipo_ContatoDTO save( Tipo_ContatoDTO tipoContato ) throws ErroAoSalvar {
        try {
            return this.saveAndEdit( tipoContato.convert() );
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Tipo_ContatoDTO edit( Tipo_ContatoDTO tipoContato, Integer id ) throws ErroAoRealizarOperacao {
        try {
            Tipo_ContatoEntity tipoContatoEntity = tipoContato.convert();
            tipoContatoEntity.setId(id);
            return this.saveAndEdit(tipoContatoEntity);
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private Tipo_ContatoDTO saveAndEdit( Tipo_ContatoEntity tipoContato ){
        Tipo_ContatoEntity tipoNovo = repository.save( tipoContato );
        return new Tipo_ContatoDTO( tipoNovo );
    }

    public Tipo_ContatoDTO findById( Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<Tipo_ContatoEntity> tipoContato = repository.findById(id);
            if ( tipoContato.isPresent()) {
                return new Tipo_ContatoDTO(repository.findById(id).get());
            }
            return null;
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<Tipo_ContatoDTO> convertList(Iterable<Tipo_ContatoEntity> entidades){
        ArrayList<Tipo_ContatoDTO> tiposConvertidos = new ArrayList<Tipo_ContatoDTO>();
        for( Tipo_ContatoEntity entidade:entidades){
            tiposConvertidos.add( new Tipo_ContatoDTO( entidade ) );
        }
        return tiposConvertidos;
    }

}
