package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.DTO.UsuarioDTOResponse;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public UsuarioDTOResponse salvar(UsuarioDTO usuario) {
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

        usuario.setSenha(bcryptEncoder.encode(usuario.getSenha()));
        return new UsuarioDTOResponse(repository.save(usuario.convert()) );
    }
}