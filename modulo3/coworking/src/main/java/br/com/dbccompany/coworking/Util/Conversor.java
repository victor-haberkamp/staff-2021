package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;

import java.time.Duration;

public class Conversor {

    public static Integer convertToMinute(Integer quantidade, Tipo_ContratacaoEnum tipoDeContratacao){
        if(tipoDeContratacao.equals(Tipo_ContratacaoEnum.MINUTO)){
            return quantidade;
        }else if( tipoDeContratacao.equals(Tipo_ContratacaoEnum.HORA) ){
            return quantidade*60;
        }else if( tipoDeContratacao.equals(Tipo_ContratacaoEnum.TURNO) ){
            return quantidade*60*5;
        }else if( tipoDeContratacao.equals(Tipo_ContratacaoEnum.DIARIA) ){
            return quantidade*60*5*2;
        }else if( tipoDeContratacao.equals(Tipo_ContratacaoEnum.SEMANA) ){
            return quantidade*60*5*2*5;
        }else if( tipoDeContratacao.equals(Tipo_ContratacaoEnum.MES) ){
            return quantidade*60*5*2*5*4;
        }else{
            return null;
        }
    }

    public static Integer convertTempoGasto( Tipo_ContratacaoEnum tipoContratacao, Duration tempoGasto ){
        if(tipoContratacao == Tipo_ContratacaoEnum.MINUTO){
            return (int) tempoGasto.toMinutes();
        }
        else if(tipoContratacao == Tipo_ContratacaoEnum.HORA){
            return (int) tempoGasto.toHours();
        }
        else if(tipoContratacao == Tipo_ContratacaoEnum.TURNO){
            return (int) tempoGasto.toHours() / 5;
        }
        else if (tipoContratacao == Tipo_ContratacaoEnum.DIARIA){
            return (int) tempoGasto.toDays();
        }
        else if(tipoContratacao == Tipo_ContratacaoEnum.SEMANA){
            return (int) tempoGasto.toDays() / 5;
        }
        else if (tipoContratacao == Tipo_ContratacaoEnum.MES){
            return (int) tempoGasto.toDays() / 20;
        }
        return null;
    }

}
