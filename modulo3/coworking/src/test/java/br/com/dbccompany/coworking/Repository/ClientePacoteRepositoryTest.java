package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ClientePacoteRepositoryTest {

    @Autowired
    public Cliente_PacoteRepository repository;

    @Autowired
    public ClientesRepository clienteRepository;

    @Autowired
    public PacoteRepository pacoteRepository;

    @Test
    public void criaClientePacoteESalva(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("013.149.860-66");
        cliente.setNome("Josias");
        cliente.setDataDeNascimento( LocalDate.parse("2000-11-05") );
        cliente = clienteRepository.save( cliente );

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(55.20);
        pacote = pacoteRepository.save(pacote);

        Cliente_PacoteEntity clientePacote = new Cliente_PacoteEntity();
        clientePacote.setPacote(pacote);
        clientePacote.setCliente(cliente);
        clientePacote.setQuantidade(10);
        clientePacote = repository.save(clientePacote);
        assertNotNull( clientePacote.getCliente() );
        assertNotNull( clientePacote.getPacote() );
        assertEquals( clientePacote.getQuantidade(), 10 );
    }

}
