package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@Profile("test")
public class ContatoTest {

    @Autowired
    private ContatoRepository repository;

    @Autowired
    private Tipo_ContatoRepository tipoContatoRepository;

    @Test
    public void salvarContato(){
        ContatoEntity contato = new ContatoEntity();
        Tipo_ContatoEntity tipoContato = new Tipo_ContatoEntity();
        tipoContato.setNome("email");
        tipoContato = tipoContatoRepository.save(tipoContato);
        contato.setTipoContato(tipoContato);
        contato.setValor("abcde@gmail.com");
        repository.save(contato);
        assertEquals( contato.getValor(), (repository.findByValor("abcde@gmail.com")).getValor() );
    }

    @Test
    public void itemNaoExistente(){
        String valor = "abacaxiVoador";
        assertNull( repository.findByValor(valor));
    }
}
