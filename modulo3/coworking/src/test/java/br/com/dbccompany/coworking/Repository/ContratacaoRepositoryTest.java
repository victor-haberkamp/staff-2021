package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ContratacaoRepositoryTest {

    @Autowired
    private ContratacaoRepository repository;

    @Test
    public void criaESalvaNovaContracatao(){
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setPrazo(10);
        contratacao.setQuantidade(15);
        contratacao.setTipoContratacao(Tipo_ContratacaoEnum.HORA);
        contratacao = repository.save( contratacao );
        assertEquals(contratacao.getTipoContratacao(), Tipo_ContratacaoEnum.HORA);
        assertEquals(contratacao.getPrazo(), 10);
        assertEquals(contratacao.getQuantidade(), 15);
    }





}
