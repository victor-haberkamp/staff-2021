package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Tipo_ContatoTest {

    @Autowired
    private Tipo_ContatoRepository repository;

    @Test
    public void salvarTipoContato(){
        Tipo_ContatoEntity tipoContato = new Tipo_ContatoEntity();
        tipoContato.setNome( "email" );
        repository.save( tipoContato );
        assertEquals( tipoContato, repository.findByNome("email") );
    }

    @Test
    public void itemNãoExistente(){
        assertNull( repository.findByNome("NomeInexistente") );
    }


}
