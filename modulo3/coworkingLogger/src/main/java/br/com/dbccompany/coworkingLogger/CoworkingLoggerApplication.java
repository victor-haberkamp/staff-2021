package br.com.dbccompany.coworkingLogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoworkingLoggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoworkingLoggerApplication.class, args);
	}

}
