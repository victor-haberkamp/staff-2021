package br.com.dbccompany.coworkingLogger.Exception;

public class ArgumentosInvalidos extends LogException{

    public ArgumentosInvalidos() {
        super("Argumentos invalidos para cadastrar erro");
    }

}
