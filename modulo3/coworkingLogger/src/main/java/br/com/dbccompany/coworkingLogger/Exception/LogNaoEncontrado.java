package br.com.dbccompany.coworkingLogger.Exception;

public class LogNaoEncontrado extends LogException {

    public LogNaoEncontrado(){
        super("Não foi possível realizar a busca!");
    }

}
