package br.com.dbccompany.coworkingLogger.Repository;

import br.com.dbccompany.coworkingLogger.Collection.Log;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LogRepository extends MongoRepository<Log, String> {

    List<Log> findAllByCodigo(String codigo );
    List<Log> findAllByTipo(String tipo );

}
