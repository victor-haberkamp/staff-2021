package br.com.dbccompany.coworkingLogger.LoggerTest;

import br.com.dbccompany.coworkingLogger.Collection.Log;
import br.com.dbccompany.coworkingLogger.Repository.LogRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
public class LoggerTest {

    @Autowired
    private LogRepository repository;

    @Test
    public void salvarNovoLogEBuscarPorCodigo(){
        Log testLog = new Log();
        testLog.setCodigo("TEST");
        testLog.setData("TEST");
        testLog.setTipo("TEST");
        testLog.setDescricao("TEST");
        repository.save(testLog);
        assertTrue( repository.findAllByCodigo("TEST").size()>0 );
    }

    @Test
    public void salvarNovoLogEBuscarPorTipo(){
        Log testLog = new Log();
        testLog.setCodigo("TEST");
        testLog.setData("TEST");
        testLog.setTipo("TEST_TIPO");
        testLog.setDescricao("TEST");
        repository.save(testLog);
        assertTrue( repository.findAllByTipo("TEST_TIPO").size()>0 );
    }

}
