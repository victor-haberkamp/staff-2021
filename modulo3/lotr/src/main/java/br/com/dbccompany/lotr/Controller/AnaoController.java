package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/anao")
public class AnaoController {

    @Autowired
    private AnaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<AnaoDTO> trazerTodosItens(){
        return  service.trazerTodosOsItensAnao();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public AnaoDTO trazerItemEspecifico( @PathVariable Integer id ){
        return service.buscarAnaoPorId( id );
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public AnaoDTO salvarItem(@RequestBody AnaoDTO anao ){
        return service.saveAnao( anao ) ;
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public AnaoDTO editarItem(@RequestBody AnaoDTO anao, @PathVariable Integer id){
        return service.editAnao( anao, id ) ;
    }

    @GetMapping( value = "/buscarTodosAnoesPorId" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorId( @RequestBody List<Integer> ids ){
        return  service.buscarTodosAnoesPorId( ids );
    }

    @GetMapping( value = "/buscarAnaoPorExperiencia/{id}" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorExperiencia( @PathVariable Integer id ){
        return  service.buscarAnaoPorExperiencia( id );
    }

    @GetMapping( value = "/buscarTodosAnoesPorExperiencia/{id}" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorExperiencia( @PathVariable Integer id ){
        return  service.buscarTodosAnoesPorExperiencia( id );
    }

    @GetMapping( value = "/buscarTodosAnoesPorExperiencia" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorExperiencia( @RequestBody List<Integer> ids ){
        return  service.buscarTodosAnoesPorExperiencia( ids );
    }

    @GetMapping( value = "/buscarAnaoPorNome" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorNome( @RequestBody String nome ){
        return  service.buscarAnaoPorNome( nome );
    }

    @GetMapping( value = "/buscarAnoesPorNome" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorNome( @RequestBody String nome ){
        return  service.buscarTodosAnoesPorNome( nome );
    }

    @GetMapping( value = "/buscarTodosAnoesPorNome" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorNome( @RequestBody List<String> nomes ){
        return  service.buscarTodosAnoesPorNome( nomes );
    }

    @GetMapping( value = "/buscarAnaoPorQtdDano" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorQtdDano ( @RequestBody Double qtdDano ){
        return  service.buscarAnaoPorQtdDano( qtdDano );
    }

    @GetMapping( value = "/buscarTodosAnoesPorQtdDano" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesQtdDano ( @RequestBody Double qtdDano ){
        return  service.buscarTodosAnoesQtdDano( qtdDano );
    }

    @GetMapping( value = "/buscarTodosAnoesPorQtdDanoEm" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesQtdDano ( @RequestBody List<Double> qtdDano ){
        return  service.buscarTodosAnoesQtdDano( qtdDano );
    }

    @GetMapping( value = "/buscarAnaoPorQtdExperienciaPorAtaque" )
    @ResponseBody
    public AnaoDTO buscarAnaoPorQtdExperienciaPorAtaque ( @RequestBody Integer qtdExperienciaPorAtaque ){
        return  service.buscarAnaoPorQtdExperienciaPorAtaque( qtdExperienciaPorAtaque );
    }

    @GetMapping( value = "/buscarTodosAnoesQtdExperienciaPorAtaque" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesQtdExperienciaPorAtaque ( @RequestBody Integer qtdExperienciaPorAtaque ){
        return  service.buscarTodosAnoesQtdExperienciaPorAtaque ( qtdExperienciaPorAtaque );
    }

    @GetMapping( value = "/buscarTodosAnoesQtdExperienciaPorAtaqueEm" )
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesQtdExperienciaPorAtaque ( @RequestBody List<Integer> qtdExperienciaPorAtaque ){
        return  service.buscarTodosAnoesQtdExperienciaPorAtaque ( qtdExperienciaPorAtaque );
    }

}
