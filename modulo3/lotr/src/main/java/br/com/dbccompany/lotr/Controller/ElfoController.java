package br.com.dbccompany.lotr.Controller;


import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ElfoController {

    @Autowired
    private ElfoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ElfoDTO> trazerTodosItens(){
        return  service.trazerTodosOsElfo();
    }

    @GetMapping( value = "/{ id }" )
    @ResponseBody
    public ElfoDTO trazerItemEspecifico( @PathVariable Integer id ){
        return service.buscarElfoPorId( id );
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ElfoDTO salvarItem(@RequestBody ElfoDTO elfo ){
        return service.saveElfo( elfo ) ;
    }

    @PutMapping( value = "/editar/ { id }")
    @ResponseBody
    public ElfoDTO editarItem(@RequestBody ElfoDTO elfo, @PathVariable Integer id){
        return service.editElfo( elfo, id ) ;
    }
}
