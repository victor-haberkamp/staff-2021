package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario" )
public class InventarioController {

    @Autowired
    private InventarioService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<InventarioDTO> trazerTodosInventarios(){
        return service.trazerTodosOsInventarios();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public InventarioDTO trazerItemEspecifico( @PathVariable Integer id ){
        return service.buscarPorId( id );
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public InventarioDTO salvarItem(@RequestBody InventarioDTO inventario ){
        return service.save( inventario );
    }

    @PutMapping ( value = "/editar/{id}")
    @ResponseBody
    public InventarioDTO editarItem( @RequestBody InventarioDTO inventario, @PathVariable Integer id){
        return service.edit( inventario, id );
    }

    @GetMapping ( value = "/encontrarTodosPorId" )
    @ResponseBody
    public List<InventarioDTO> encontrarTodosPorId ( @RequestBody List<Integer> ids) {
        return service.findAllByIdIn( ids );
    }



}
