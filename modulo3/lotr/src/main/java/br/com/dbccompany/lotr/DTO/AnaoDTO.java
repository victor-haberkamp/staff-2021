package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;

public class AnaoDTO extends PersonagemDTO{

    public AnaoDTO(AnaoEntity anao) {
        super(anao);
    }


    public AnaoDTO(){}

    public AnaoEntity convert( ){
        AnaoEntity anao = new AnaoEntity( this.nome );
        if( super.getId() != null ) {
            anao.setId(super.getId());
        }
        if( super.getExperiencia() != null ) {
            anao.setExperiencia( super.getExperiencia() );
        }
        if( super.getVida() != null ) {
            anao.setVida( super.getVida() );
        }
        if( super.getQtdDano() != null) {
            anao.setQtdDano(super.getQtdDano());
        }
        if( super.getQtdExperienciaPorAtaque() != null) {
            anao.setQtdExperienciaPorAtaque(super.getQtdExperienciaPorAtaque());
        }
        if( super.getStatus() != null) {
            anao.setStatus(super.getStatus());
        }
        if( super.getInventario() != null ){
            anao.setInventario( super.getInventario().converter() );
            //anao.getInventario().setPersonagem( anao );
        }
        return anao;
    }
}
