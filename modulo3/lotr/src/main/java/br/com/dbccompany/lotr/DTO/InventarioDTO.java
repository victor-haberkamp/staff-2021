package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.*;

import java.util.ArrayList;
import java.util.List;

public class InventarioDTO {
    private Integer id;
    private List<Inventario_X_ItemDTO> inventarioItem;
    private PersonagemDTO personagem;

    public InventarioDTO( InventarioEntity inventarioEntity){
        if(inventarioEntity != null){
            if( inventarioEntity.getId() != null ){
                this.id = inventarioEntity.getId();
            }
            if( inventarioEntity.getInventarioItem() != null ) {
                this.inventarioItem = this.convertList(inventarioEntity.getInventarioItem());
            }else {
                this.inventarioItem = null;
            }
            PersonagemEntity personagem = inventarioEntity.getPersonagem();
            if( personagem instanceof AnaoEntity){
                this.personagem =  new AnaoDTO( (AnaoEntity) inventarioEntity.getPersonagem() );
            }else if( personagem instanceof ElfoEntity){
                this.personagem =  new ElfoDTO( (ElfoEntity) inventarioEntity.getPersonagem() );
            }
        }
    }

    public InventarioDTO() {
    }

    public InventarioEntity converter(){
        InventarioEntity inventarioEntity = new InventarioEntity();
        if( inventarioItem == null && personagem == null){
            return inventarioEntity;
        }
        inventarioEntity.setInventarioItem( this.convertListBack( this.inventarioItem ) );
        inventarioEntity.setPersonagem(personagem.convert() );
        return inventarioEntity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonagemDTO getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemDTO personagem) {
        this.personagem = personagem;
    }

    public List<Inventario_X_ItemDTO> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemDTO> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    private List<Inventario_X_ItemDTO> convertList(List<Inventario_X_ItemEntity> entidades){
        ArrayList<Inventario_X_ItemDTO> convertidos = new ArrayList<>();
        for( Inventario_X_ItemEntity entidade:entidades ){
            convertidos.add( new Inventario_X_ItemDTO( entidade ) );
        }
        return convertidos;
    }

    private List<Inventario_X_ItemEntity> convertListBack(List<Inventario_X_ItemDTO> dtos){
        ArrayList<Inventario_X_ItemEntity> convertidos = new ArrayList<>();
        for( Inventario_X_ItemDTO dto:dtos ){
            convertidos.add( dto.converter() );
        }
        return convertidos;
    }
}