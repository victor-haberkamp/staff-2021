package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;

public class Inventario_X_ItemDTO {
    private ItemDTO item;
    private int quantidade;

    public Inventario_X_ItemDTO(Inventario_X_ItemEntity inventarioXItemEntity) {
        this.item = new ItemDTO( inventarioXItemEntity.getItem() );
        this.quantidade = inventarioXItemEntity.getQuantidade();
    }

    public Inventario_X_ItemDTO() {
    }

    public Inventario_X_ItemEntity converter(){
        Inventario_X_ItemEntity inventarioItem = new Inventario_X_ItemEntity();
        inventarioItem.setItem( item.converter() );
        inventarioItem.setQuantidade( quantidade );
        return inventarioItem;
    }

    public ItemDTO getItem() {
        return item;
    }

    public void setItem(ItemDTO item) {
        this.item = item;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
