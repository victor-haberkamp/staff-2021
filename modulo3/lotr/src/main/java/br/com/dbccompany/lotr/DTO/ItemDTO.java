package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ItemEntity;

public class ItemDTO {
    private String descricao;

    public ItemDTO(ItemEntity item){
        this.descricao = item.getDescricao();
    }

    public ItemDTO(String descricao){
        this.descricao = descricao;
    }

    public ItemEntity converter(){
        ItemEntity item = new ItemEntity();
        item.setDescricao(this.descricao);
        return item;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
