package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;


public abstract class PersonagemDTO {
    protected Integer id;
    protected String nome;
    protected Integer experiencia;
    protected Double vida;
    protected Double qtdDano;
    protected Integer qtdExperienciaPorAtaque;
    protected StatusEnum status;
    protected InventarioDTO inventario;


    public PersonagemDTO(PersonagemEntity personagem) {
        //if( personagem.getId() != null ) {
            this.id = personagem.getId();
        //}
        this.nome = personagem.getNome();
        this.experiencia = personagem.getExperiencia();
        this.vida = personagem.getVida();
        this.qtdDano = personagem.getQtdDano();
        this.qtdExperienciaPorAtaque = personagem.getQtdExperienciaPorAtaque();
        this.status = personagem.getStatus();
        this.inventario = new InventarioDTO ( personagem.getInventario() );
    }

    public PersonagemDTO(){};

    public abstract PersonagemEntity convert();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioDTO getInventario() {
        return inventario;
    }

    public void setInventario(InventarioDTO inventario) {
        this.inventario = inventario;
    }
}

