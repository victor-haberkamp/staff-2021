package br.com.dbccompany.lotr.Entity;


import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class Inventario_X_ItemId implements Serializable {
    private int id_inventario;
    private int id_item;

    public Inventario_X_ItemId(int id_inventario, int id_item) {
        this.id_inventario = id_inventario;
        this.id_item = id_item;
    }
}
