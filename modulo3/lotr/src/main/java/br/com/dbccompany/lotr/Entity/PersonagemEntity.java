package br.com.dbccompany.lotr.Entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorValue( value = "personagem")
public abstract class PersonagemEntity {

    public PersonagemEntity(String nome){
        this.nome = nome;
        this.status = StatusEnum.RECEM_CRIADO;
    }

    public PersonagemEntity(){};

    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue( generator= "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(nullable = false)
    private int experiencia = 0;

    @Column(nullable = false, precision = 4, scale = 2 )
    private double vida;

    @Column(nullable = false, precision = 4, scale = 2)
    private double qtdDano = 0.;

    @Column(nullable = false)
    private int qtdExperienciaPorAtaque = 1;

    @Enumerated( EnumType.STRING )
    private StatusEnum status;

    @JsonIgnore
    @OneToOne( cascade = CascadeType.ALL, mappedBy = "personagem")
    @JoinColumn( name = "id_inventario", referencedColumnName = "id")
    private InventarioEntity inventario;
/*
    @PrePersist
    @PreUpdate
    public void updateInventarioAssociation(){
        this.inventario.setPersonagem(this);
    }
*/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public int getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(int qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
