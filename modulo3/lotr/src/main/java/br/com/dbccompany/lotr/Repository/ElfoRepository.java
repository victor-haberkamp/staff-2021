package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.ElfoEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface ElfoRepository extends PersonagemRepository<ElfoEntity>{

}
