package br.com.dbccompany.lotr.Security;

import br.com.dbccompany.lotr.Entity.UserEntity;
import br.com.dbccompany.lotr.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AutenticationService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByUsername( username );
        if(user == null){
            throw new UsernameNotFoundException( username );
        }
        return user;
    }
}
