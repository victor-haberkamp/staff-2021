package br.com.dbccompany.lotr.Security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AutenticationService autenticationService;

    @Override
    protected void configure( HttpSecurity http ) throws Exception {
        http.headers().frameOptions().sameOrigin().and().cors().and()
                .csrf().disable().authorizeRequests()
                //.antMatchers( HttpMethod.GET, "/login" ).permitAll()
                .antMatchers("/api/**").permitAll()
                .anyRequest().authenticated().and()
                .addFilterBefore( new JWTLoginFilter("/login", authenticationManager() ),
                        UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore( new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class );
    }

    @Override
    protected void configure ( AuthenticationManagerBuilder auth ) throws Exception {
         auth.userDetailsService( autenticationService );
    }

}
