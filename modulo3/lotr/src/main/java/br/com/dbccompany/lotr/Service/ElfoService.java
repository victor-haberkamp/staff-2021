package br.com.dbccompany.lotr.Service;


import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService extends PersonagemService<ElfoRepository, ElfoEntity> {

    public List<ElfoDTO> trazerTodosOsElfo(){
        return this.convertList( super.trazerTodosOsPersonagens() );
    }

    public ElfoDTO saveElfo( ElfoDTO elfo ){
        return new ElfoDTO( super.save( elfo.convert() ) );
    }

    public ElfoDTO editElfo ( ElfoDTO elfo, Integer id ){
        return new ElfoDTO( super.edit( elfo.convert(), id ));
    }

    public ElfoDTO buscarElfoPorId( Integer id ){
        return new ElfoDTO( super.buscarPorId( id ) );
    }

    public List<ElfoDTO> buscarTodosElfosPorId( List<Integer> ids ){
        return this.convertList( findAllByIdIn( ids ) );
    }

    public ElfoDTO buscarElfoPorExperiencia( Integer id ){
        return new ElfoDTO( super.findByExperiencia( id ) );
    }

    public List<ElfoDTO> buscarTodosElfosPorExperiencia( Integer id ){
        return this.convertList( findAllByExperiencia( id ) );
    }

    public List<ElfoDTO> buscarTodosElfoPorExperiencia( List<Integer> ids ){
        return this.convertList( findAllByExperienciaIn( ids ) );
    }

    public ElfoDTO buscarElfoPorNome( String nome ){
        return new ElfoDTO( super.findByNome( nome ) );
    }

    public List<ElfoDTO> buscarTodosElfosPorNome( String nome ){
        return this.convertList( findAllByNome( nome ) );
    }

    public List<ElfoDTO> buscarTodosElfoPorNome( List<String> nomes ){
        return this.convertList( findAllByNomeIn( nomes ) );
    }

    public ElfoDTO buscarElfoPorQtdDano ( Double qtdDano ){
        return new ElfoDTO( super.findByQtdDano( qtdDano ) );
    }

    public List<ElfoDTO> buscarTodosElfosQtdDano ( Double qtdDano ){
        return this.convertList( findAllByQtdDano( qtdDano ) );
    }

    public List<ElfoDTO> buscarTodosElfosQtdDano ( List<Double> qtdDano ){
        return this.convertList( findAllByQtdDanoIn( qtdDano ) );
    }

    public ElfoDTO buscarElfoPorQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque){
        return new ElfoDTO( super.findByQtdExperienciaPorAtaque( qtdExperienciaPorAtaque ) );
    }

    public List<ElfoDTO> buscarTodosElfosQtdExperienciaPorAtaque (Integer qtdExperienciaPorAtaque){
        return this.convertList( findAllByQtdExperienciaPorAtaque (qtdExperienciaPorAtaque) );
    }

    public List<ElfoDTO> buscarTodosElfosQtdExperienciaPorAtaque (List<Integer> qtdExperienciaPorAtaque){
        return this.convertList( findAllByQtdExperienciaPorAtaqueIn ( qtdExperienciaPorAtaque ) );
    }

    private List<ElfoDTO> convertList ( List<ElfoEntity> entidades){
        ArrayList<ElfoDTO> elfosConvertidos = new ArrayList<>();
        for ( ElfoEntity entidade:entidades){
            elfosConvertidos.add( new ElfoDTO( entidade ) );
        }
        return elfosConvertidos;
    }


}
