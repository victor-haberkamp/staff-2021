package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository repository;

    public List<InventarioDTO> trazerTodosOsInventarios(){
        return this.converterList((List<InventarioEntity>) repository.findAll());
    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO save( InventarioDTO inventario ){
        return new InventarioDTO (this.saveAndEdit( inventario.converter() ) );
    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO edit( InventarioDTO inventario, Integer id ){
        InventarioEntity inventarioEntity = inventario.converter();
        inventarioEntity.setId(id);
        return new InventarioDTO ( this.saveAndEdit( inventarioEntity ) );
    }

    private InventarioEntity saveAndEdit( InventarioEntity inventario ){
        return ( repository.save( inventario ) );
    }

    public InventarioDTO buscarPorId( Integer id){
        Optional<InventarioEntity> inventario = repository.findById(id);
        if( inventario.isPresent() ){
            return new InventarioDTO( repository.findById(id).get() );
        }
        return null;
    }

    public List<InventarioDTO> findAllByIdIn (List<Integer> ids){
        return this.converterList( repository.findAllByIdIn( ids ) );
    }

    public InventarioDTO findByPersonagem ( PersonagemEntity personagem ){
        return new InventarioDTO( repository.findByPersonagem( personagem ) );
    }

    public List<InventarioEntity> findAllByIdPersonagem ( PersonagemEntity personagem ){
        return repository.findAllByPersonagem( personagem);
    }

    public List<InventarioEntity> findAllByIdPersonagemIn (List<PersonagemEntity> personagem){
        return repository.findAllByPersonagemIn( personagem );
    }

    private List<InventarioDTO> converterList(List<InventarioEntity> entidades){
        ArrayList<InventarioDTO> convertidos = new ArrayList<>();
        for( InventarioEntity entidade:entidades ){
            convertidos.add( new InventarioDTO( entidade ) );
        }
        return convertidos;
    }
}
