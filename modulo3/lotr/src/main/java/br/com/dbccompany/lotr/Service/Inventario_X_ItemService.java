package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {

    @Autowired
    private Inventario_X_ItemRepository repository;

    public List<Inventario_X_ItemDTO> trazerTodosOsItens(){
        return this.converterList( (List<Inventario_X_ItemEntity>) repository.findAll() );
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO save( Inventario_X_ItemDTO inventarioItem ){
        return this.saveAndEdit( inventarioItem.converter() );
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO edit( Inventario_X_ItemDTO inventarioItem, Inventario_X_ItemId id ){
        Inventario_X_ItemEntity inventarioItemEntity = inventarioItem.converter();
        inventarioItemEntity.setId(id);
        return this.saveAndEdit( inventarioItemEntity );
    }

    private Inventario_X_ItemDTO saveAndEdit( Inventario_X_ItemEntity inventarioItem ){
        return new Inventario_X_ItemDTO( repository.save( inventarioItem ) );
    }

    public Inventario_X_ItemDTO buscarPorId( Inventario_X_ItemId id ){
        Optional<Inventario_X_ItemEntity> item = repository.findById(id);
        if( item.isPresent() ){
            return new Inventario_X_ItemDTO( repository.findById(id).get() );
        }
        return null;
    }

    public List<Inventario_X_ItemDTO> findAllByIdIn (List<Inventario_X_ItemId> id){
        return this.converterList( repository.findAllByIdIn( id ) );
    }

    public Inventario_X_ItemDTO findByInventario(Integer idInventario){
        return new Inventario_X_ItemDTO( repository.findByInventario( idInventario ) );
    }

    public List<Inventario_X_ItemDTO> findAllByInventario ( Integer idInventario){
        return this.converterList( repository.findAllByInventario( idInventario ) );
    }

    public List<Inventario_X_ItemDTO> findAllByInventarioIn ( List<Integer> idsInventario){
        return this.converterList( repository.findAllByInventarioIn( idsInventario ) );
    }

    public Inventario_X_ItemDTO findByItem(Integer idItem){
        return new Inventario_X_ItemDTO( repository.findByItem( idItem ) );
    }

    public List<Inventario_X_ItemDTO> findAllByItem (Integer idItem){
        return this.converterList( repository.findAllByItem( idItem ) );
    }

    public List<Inventario_X_ItemDTO> findAllByItemIn (List<Integer> idsItem){
        return this.converterList( repository.findAllByItemIn( idsItem) );
    }

    private List<Inventario_X_ItemDTO> converterList(List<Inventario_X_ItemEntity> entidades){
        ArrayList<Inventario_X_ItemDTO> convertidos = new ArrayList<>();
        for( Inventario_X_ItemEntity entidade:entidades ){
            convertidos.add( new Inventario_X_ItemDTO( entidade ) );
        }
        return convertidos;
    }

}
