/*
use local

db.createCollection("pais");

db.pais.insert({
    nome: "Brasil"
});
db.pais.insert(
{
    nome: "EUA"
});
db.pais.insert(
{
    nome: "England"
});
db.pais.insert(
{
    nome: "Argentina"
});

//db.pais.find()

db.createCollection("estado");
db.estado.insert({
    nome: "NA",
    pais: ObjectId("60bfb305466b4a3c9ee9c829")
});
db.estado.insert({
    nome: "California",
    pais: ObjectId("60bfb305466b4a3c9ee9c82a")
});
db.estado.insert({
    nome: "Boroughs",
    pais: ObjectId("60bfb305466b4a3c9ee9c82b")
});
db.estado.insert({
    nome: "Sao Paulo",
    pais: ObjectId("60bfb305466b4a3c9ee9c829")
});
db.estado.insert({
    nome: "Buenos Aires",
    pais: ObjectId("60bfb305466b4a3c9ee9c82c")
});
db.estado.find()

db.createCollection("cidade");
db.cidade.insert({
    nome: "NA",
    estado: ObjectId("60bfb4a9466b4a3c9ee9c82d")
});
db.cidade.insert({
    nome: "San Franscisco",
    estado: ObjectId("60bfb4a9466b4a3c9ee9c82e")
});
db.cidade.insert({
    nome: "Londres",
    estado: ObjectId("60bfb4a9466b4a3c9ee9c82f")
});
db.cidade.insert({
    nome: "Itu",
    estado: ObjectId("60bfb4a9466b4a3c9ee9c830")
});
db.cidade.insert({
    nome: "Buenos Aires",
    estado: ObjectId("60bfb4a9466b4a3c9ee9c831")
});
db.cidade.find();

db.createCollection("bairro");
db.bairro.insert({
    nome: "NA",
    cidade: ObjectId("60bfb5bd466b4a3c9ee9c832")
});
db.bairro.insert({
    nome: "Between Hyde and owell Streets",
    cidade: ObjectId("60bfb5bd466b4a3c9ee9c833")
});
db.bairro.insert({
    nome: "Croydon",
    cidade: ObjectId("60bfb5bd466b4a3c9ee9c834")
});
db.bairro.insert({
    nome: "Qualquer",
    cidade: ObjectId("60bfb5bd466b4a3c9ee9c835")
});
db.bairro.insert({
    nome: "Caminito",
    cidade: ObjectId("60bfb5bd466b4a3c9ee9c836")
});
db.bairro.find();

db.createCollection("endereco");

db.endereco.insert(
{
    rua: "Testando",
    numero: "55",
    complemento: "loja 1",
    bairro: ObjectId("60bfb78b466b4a3c9ee9c837")

} );
db.endereco.insert(
{
   rua: "Testing",
   numero: "122",
   complemento: "",
   bairro: ObjectId("60bfb78b466b4a3c9ee9c838")
} );
db.endereco.insert(
{
    rua: "Tesing",
    numero: "525",
    complemento: "",
    bairro: ObjectId("60bfb78b466b4a3c9ee9c839")
} );
db.endereco.insert(
{
    rua: "Testando",
    numero: "55",
    complemento: "loja 2",
    bairro: ObjectId("60bfb78b466b4a3c9ee9c837")
} );

db.endereco.insert(
{
    rua: "Testando",
    numero: "55",
    complemento: "loja 3",
    bairro: ObjectId("60bfb78b466b4a3c9ee9c837")
} );
db.endereco.insert(
{
    rua: "Rua do Meio",
    numero: "2233",
    complemento: "",
    bairro: ObjectId("60bfb78b466b4a3c9ee9c83a")
} );
db.endereco.insert(
{
    rua: "Rua do boca",
    numero: "222",
    complemento: "",
    bairro: ObjectId("60bfb78b466b4a3c9ee9c83b")
} );
db.endereco.find();

db.createCollection("consolidacao");
db.consolidacao.insert({
   saldoAtual: 0,
   saques: 0,
   depositos: 0,
   numeroDeCorrentistas: 0
});
db.consolidacao.find();

db.createCollection("cliente");
db.endereco.find();

db.cliente.insert({
    nome: "cliente1",
    cpf: "000.000.000-01",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente2",
    cpf: "000.000.000-02",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente3",
    cpf: "000.000.000-03",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente4",
    cpf: "000.000.000-04",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente5",
    cpf: "000.000.000-05",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente6",
    cpf: "000.000.000-06",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente7",
    cpf: "000.000.000-07",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente8",
    cpf: "000.000.000-08",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente9",
    cpf: "000.000.000-09",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente10",
    cpf: "000.000.000-10",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente11",
    cpf: "000.000.000-11",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente12",
    cpf: "000.000.000-12",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente13",
    cpf: "000.000.000-13",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente14",
    cpf: "000.000.000-14",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente15",
    cpf: "000.000.000-15",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente16",
    cpf: "000.000.000-16",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente17",
    cpf: "000.000.000-17",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente18",
    cpf: "000.000.000-18",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente19",
    cpf: "000.000.000-19",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente20",
    cpf: "000.000.000-20",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente21",
    cpf: "000.000.000-21",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente22",
    cpf: "000.000.000-22",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente23",
    cpf: "000.000.000-23",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente24",
    cpf: "000.000.000-24",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente25",
    cpf: "000.000.000-25",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente26",
    cpf: "000.000.000-26",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente27",
    cpf: "000.000.000-27",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente28",
    cpf: "000.000.000-28",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente29",
    cpf: "000.000.000-29",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.insert({
    nome: "cliente30",
    cpf: "000.000.000-30",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});

db.cliente.insert({
    nome: "gerente1",
    cpf: "000.000.001-01",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: ObjectId("60bfb8a2466b4a3c9ee9c83c")
});
db.cliente.find();

db.createCollection("gerentes");
db.gerentes.insert({
    dadosGerente: ObjectId("60bfba2a466b4a3c9ee9c840"),
    codigoFuncionario: "0001",
    tipoGerente: "GC"
} ) ;
db.gerentes.find();

db.createCollection("movimentacoes");

db.createCollection("contas");

db.contas.insert({
    codigo: 0001,
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes: [],
    clientes: [
    ObjectId("60bfba2a466b4a3c9ee9c840"), 
    ObjectId("60bfc2e4466b4a3c9ee9c845"),
    ObjectId("60bfc2e4466b4a3c9ee9c846")],
    gerentes: [ObjectId("60bfbc73466b4a3c9ee9c842")] 
} );
db.contas.insert({
    codigo: 0002,
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes: [],
    clientes: [
    ObjectId("60bfc881466b4a3c9ee9c850"), 
    ObjectId("60bfc881466b4a3c9ee9c851")],
    gerentes: [ObjectId("60bfbc73466b4a3c9ee9c842")] 
} );

db.contas.insert({
    codigo: 0003,
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes: [],
    clientes: [
    ObjectId("60bfc881466b4a3c9ee9c850"), 
    ObjectId("60bfc881466b4a3c9ee9c851")],
    gerentes: [ObjectId("60bfbc73466b4a3c9ee9c842")] 
} );
db.contas.insert({
    codigo: 0004,
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes: [],
    clientes: [
    ObjectId("60bfc881466b4a3c9ee9c84e"), 
    ObjectId("60bfc881466b4a3c9ee9c84f")],
    gerentes: [ObjectId("60bfbc73466b4a3c9ee9c842")] 
} );
db.contas.insert({
    codigo: 0005,
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes: [],
    clientes: [
    ObjectId("60bfc2e4466b4a3c9ee9c848"), 
    ObjectId("60bfc881466b4a3c9ee9c84d")],
    gerentes: [ObjectId("60bfbc73466b4a3c9ee9c842")] 
} );
db.contas.insert({
    codigo: 0006,
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes: [],
    clientes: [ ObjectId("60bfba2a466b4a3c9ee9c840") ],
    gerentes: [ObjectId("60bfbc73466b4a3c9ee9c842")] 
} );
db.contas.insert({
    codigo: 0007,
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes: [],
    clientes: [ ObjectId("60bfbb68466b4a3c9ee9c841") ],
    gerentes: [ObjectId("60bfbc73466b4a3c9ee9c841")] 
} );
db.contas.insert({
    codigo: 0008,
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes: [],
    clientes: [ ObjectId("60bfc2e4466b4a3c9ee9c846") ],
    gerentes: [ ObjectId("60bfbb68466b4a3c9ee9c841")] 
} );


db.contas.find();

db.createCollection("agencias");
db.agencias.insert( {
    codigo: "0001",
    nome: "Web",
    endereco : ObjectId("60bfb8a2466b4a3c9ee9c83c"),
    consolidacao: [ ObjectId("60bfb026466b4a3c9ee9c825") ],
    contas:[ ObjectId("60bfc098466b4a3c9ee9c843") ]
} );

//db.agencias.find();
db.agencias.update( { _id: ObjectId("60bfc22b466b4a3c9ee9c844") }, { $set:  { contas: [ObjectId("60bfc098466b4a3c9ee9c843"), ObjectId("60bfcc66466b4a3c9ee9c867")]  } } );
db.agencias.insert( {
    codigo: "0002",
    nome: "California",
    endereco : ObjectId("60bfb972466b4a3c9ee9c83d"),
    consolidacao: [ ObjectId("60bfb026466b4a3c9ee9c825") ],
    contas:[ ObjectId("60bfcc66466b4a3c9ee9c867"), ObjectId("60bfcc66466b4a3c9ee9c868") ]
} );
db.agencias.insert( {
    codigo: "0101",
    nome: "Londres",
    endereco : ObjectId("60bfb8a2466b4a3c9ee9c83c"),
    consolidacao: [ ObjectId("60bfb026466b4a3c9ee9c825") ],
    contas:[ ObjectId("60bfcc66466b4a3c9ee9c869"), ObjectId("60bfcc66466b4a3c9ee9c86a"), ObjectId("60bfcc66466b4a3c9ee9c86b"), ObjectId("60bfcc66466b4a3c9ee9c86c") ]
} );
    
db.createCollection("banco");
db.banco.insert({
   codigo: "011",
   nome: "Alfa",
   agencia: [ ObjectId("60bfc22b466b4a3c9ee9c844"),ObjectId("60bfd040466b4a3c9ee9c86d"),ObjectId("60bfd085466b4a3c9ee9c86e") ] 
} );

db.agencias.insert( {
    codigo: "0001",
    nome: "Web",
    endereco : ObjectId("60bfb972466b4a3c9ee9c83f"),
    consolidacao: [ ObjectId("60bfb026466b4a3c9ee9c825") ],
    contas:[ ObjectId("60bfc098466b4a3c9ee9c843"), ObjectId("60bfcc66466b4a3c9ee9c867") ]
} );

db.banco.insert({
   codigo: "241",
   nome: "Beta",
   agencia: [ ObjectId("60bfd1c4466b4a3c9ee9c870") ] 
} );

db.agencias.insert( {
    codigo: "0001",
    nome: "Web",
    endereco : ObjectId("60bfc744466b4a3c9ee9c84a"),
    consolidacao: [ ObjectId("60bfb026466b4a3c9ee9c825") ],
    contas:[ ObjectId("60bfc098466b4a3c9ee9c843"), ObjectId("60bfcc66466b4a3c9ee9c867") ]
} );
db.agencias.insert( {
    codigo: "8761",
    nome: "Itu",
    endereco : ObjectId("60bfc744466b4a3c9ee9c84b"),
    consolidacao: [ ObjectId("60bfb026466b4a3c9ee9c825") ],
    contas:[ ObjectId("60bfcc66466b4a3c9ee9c867"), ObjectId("60bfcc66466b4a3c9ee9c868") ]
} );
db.agencias.insert( {
    codigo: "4567",
    nome: "Hermana",
    endereco : ObjectId("60bfc744466b4a3c9ee9c84c"),
    consolidacao: [ ObjectId("60bfb026466b4a3c9ee9c825") ],
    contas:[ ObjectId("60bfcc66466b4a3c9ee9c869"), ObjectId("60bfcc66466b4a3c9ee9c86a"), ObjectId("60bfcc66466b4a3c9ee9c86b"), ObjectId("60bfcc66466b4a3c9ee9c86c") ]
} );

db.banco.insert({
   codigo: "307",
   nome: "Omega",
   agencia: [ ObjectId("60bfd2cc466b4a3c9ee9c871"), ObjectId("60bfd2cc466b4a3c9ee9c872"), ObjectId("60bfd2cc466b4a3c9ee9c873") ] 
} );
*/

db.banco.find();