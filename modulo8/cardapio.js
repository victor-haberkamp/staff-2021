function cardapioIFood( veggie = true, comLactose = false ) {
  let cardapio = [
    {
        nome: 'enroladinho de salsicha',
        veggie: false,
        comLactose: false
    },
    {
        nome: 'cuca de uva',
        veggie: true,
        comLactose: false
    },
    {
        nome: 'pastel de queijo',
        veggie: true,
        comLactose: true
    },
    {
        nome: 'empada de legumes marabijosa',
        veggie: true,
        comLactose: true
    }
  ];

  if( veggie ){
      for (let i = 0; i < cardapio.length; i++) {
          if ( cardapio[i].veggie ? false : true ){
            cardapio.splice( i, 1 );
            i--;
          }
      }
  }
  
  let resultadoFinal = [];
  for (let i = 0; i < cardapio.length; i++) {
    resultadoFinal[i] = cardapio[i].nome.toUpperCase();   
  }

  return resultadoFinal;
}

console.log( cardapioIFood( ) )  // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]