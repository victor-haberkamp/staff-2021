/* Lista 1 */

//exercicio 1
/*
function calcularCirculo( objetoRecebido ){
  if( objetoRecebido.tipoCalculo == "A" ){
    return Math.PI*Math.pow( objetoRecebido.raio, 2 );
  }else if( objetoRecebido.tipoCalculo == "C" ){
    return Math.PI*objetoRecebido.raio*2;
  }else{
    return "Não foi possivel realizar a operacao";
  }
}
*/

function calcularCirculo ({ raio, tipoCalculo:tipo }){
  return Math.ceil(  tipo =="A" ? Math.PI * Math.pow( raio, 2 ) : 2 * Math.PI * raio );
}



//exercicio 2
function naoBissexto(ano){
  return ( ano%400 == 0 ) || ( ano%4 == 0 && ano%100 !=0 ) ? false : true ;
}

//exercicio 3
function somarPares( array ){
  let soma = 0;
  for (let i = 0; i < array.length; i+=2) {
     soma += array[i];
  }
  return soma;
}

//exercicio 4
/*
function adicionar(value1) {
  function adicionarInterno(value2) {
  return value1 + value2;
  }
  
  return adicionarInterno;
}
*/

let adicionar = valor1 => valor2 => valor1 + valor2

// nomeDaFuncao = (Parametro1, parametro2) => procedimentoDeRetorno

//exercicio 5

function imprimirBRL(numero){
  numero = numero.toFixed(2);
  let retorno = parseFloat(numero).toLocaleString('pt-BR');

  let retornoSplit = retorno.split(',');

  if( retornoSplit.length === 1 ){
    return retorno += ",00";
  }else{
    parteDecimal = retornoSplit[1].split('');
    if(parteDecimal.length === 1 ){
      retorno += "0";
      return retorno;
    }
    else{
      return retorno;
    }
  }

}