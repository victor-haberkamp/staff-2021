class Jogador {
  constructor( nome , numero ){
    this._nome = nome;
    this._numero = numero;
  }

  get nome() {
    return this._nome;
  }

  get numero() {
    return this._numero;
  }
}

class Time {
  constructor ( nome, tipoDeEsporte, liga ){
    this._nome = nome;
    this._tipoDeEsporte = tipoDeEsporte;
    this._status = "Ativo";
    this._liga = liga;
    this._jogadores = [];
  }

  get nome(){
    return this._nome; 
  }

  get tipoDeEsporte(){
    return this._tipoDeEsporte; 
  }

  get status(){
    return this._status; 
  }

  get liga(){
    return this._liga; 
  }

  adicionarJogador( jogador ){
    this._jogadores.push( jogador );
  }

  buscarPorNome( nomeBuscado ){
    return this._jogadores.find( jogador=> jogador.nome == nomeBuscado );
  }

  buscarPorNumero( numBuscado ){
    return this._jogadores.find( jogador=> jogador.numero == numBuscado );
  }

}

let timeExemplo = new Time( 'Time Exemplo', 'EsporteExemplo', 'StatusExemplo', 'LigaExemplo' );

console.log( timeExemplo );
timeExemplo.adicionarJogador( "Rodolfo", 7 );
console.log(timeExemplo.buscarPorNome("Rodolfo"));
console.log(timeExemplo.buscarPorNumero(7));
