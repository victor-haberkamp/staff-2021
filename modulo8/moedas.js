const moedas = ( function () {
  //função privada, somente executada no escopo local.
  function imprimirMoeda ( parametros ){

    function arredondar( numero, precisao = 2) {
      const fator = Math.pow( 10, precisao );
      return Math.ceil( numero * fator ) / fator;
    }

    const{
      numero,
      separadorMilhar,
      separadorDecimal,
      colocarMoeda,
      colocarNegativo
    } = parametros;

    //metodo de tratamento do valor;
    const qtdCasasMilhares = 3;
    let StringBuffer = [];
    let parteDecimal = arredondar( Math.abs( numero )%1 );
    let parteInteira = Math.trunc( numero );
    let parteInteiraString = Math.abs( parteInteira ).toString();
    let tamanhoParteinteira = parteInteiraString.length;

    //
    let contador = 1;
    while ( parteInteiraString.length > 0 ) {
      if( contador % qtdCasasMilhares ==0 && parteInteiraString.length ){
        StringBuffer.push( `${separadorMilhar}${ parteInteiraString.slice( tamanhoParteInteira - contador ) }` );
        parteInteiraString = parteInteiraString.slice( 0, tamanhoParteInteira - contador );
      }else {
        StringBuffer.push( parteInteiraString );
        parteInteiraString = '';
      }
      contador++;
    }
    StringBuffer.push( parteInteiraString );

    let decimalString = parteDecimal.toString().replace( '0.', '' ).padStart(2,0);
    const numeroFormatado = `${ StringBuffer.reverse().join('') }${ separadorDecimal }${ decimalString }`;

    return parteInteira>=0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);
  }

  //Funcao publica
  return{
    imprimirBRL: ( numero ) => 
      imprimirMoeda( {
        numero, 
        separadorDecimal: ',',
        separadorMilhar: '.',
        colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
        colocarNegativo: numeroFormatado => `-${ colocarMoeda(numeroFormatado) }`
      } ),
    imprimirUSD: ( numero ) => 
      imprimirMoeda( {
        numero, 
        separadorDecimal: '.',
        separadorMilhar: ',',
        colocarMoeda: numeroFormatado => `US$ ${ numeroFormatado }`,
        colocarNegativo: numeroFormatado => `-${ colocarMoeda(numeroFormatado) }`
      } ),
    imprimirGBR: ( numero ) => 
      imprimirMoeda( {
        numero, 
        separadorDecimal: '.',
        separadorMilhar: ',',
        colocarMoeda: numeroFormatado => `£ ${ numeroFormatado }`,
        colocarNegativo: numeroFormatado => `-${ colocarMoeda(numeroFormatado) }`
      } ),
    imprimirEUR: ( numero ) => 
      imprimirMoeda( {
        numero, 
        separadorDecimal: ',',
        separadorMilhar: '.',
        colocarMoeda: numeroFormatado => `${ numeroFormatado } €`,
        colocarNegativo: numeroFormatado => `-${ colocarMoeda(numeroFormatado) }`
      } )
  };
})()