class Pokemon {
  constructor( objDaApi ) {
    this._nome = objDaApi.name;
    this._image = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._id = objDaApi.id;
    this._peso = objDaApi.weight;
    this._tipos = objDaApi.types;
    this._estatisticas = objDaApi.stats;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._image;
  }

  get altura() {
    return `${ this._altura * 10 }cm`;
  }

  get id(){
    return this._id;
  }

  get peso(){
    return `${ this._peso / 10 }kg`;
  }

  get tipos(){
    return this._tipos;
  }

  get estatisticas(){
    return this._estatisticas;
  }
}