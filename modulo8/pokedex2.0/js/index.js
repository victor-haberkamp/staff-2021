const inputBuscaPorId = document.getElementById( 'buscar-por-id' );
const feelingLuckyButton = document.getElementById( 'feeling-lucky-button' );
const clearButton = document.getElementById( 'clear-button' );
const voltarParaTopoButton = document.getElementById( 'voltar-ao-topo-button' );
const pokebolaImages = document.querySelectorAll( '.pokebola-image' );
const dadosPokemon = document.getElementById( 'dadosPokemon' );
const pokeApi = new PokeApi();

const scrollToTheEnd = () => {
  setTimeout( () => {
    window.scrollBy( {
      top: 1000,
      behavior: 'smooth',
    } )
  }, 250 );
}

const scrollToTheTop = () => {
  window.scrollBy( {
    top: -1000,
    behavior: 'smooth',
  } );
}

const getIdOnScreen = () => {
  const idAtual = dadosPokemon.querySelector( '.id' ).innerHTML;
  return ( idAtual.split( ' ' ) )[1];
}

const podeBuscarComSorte = ( number ) => {
  let cache = localStorage.getItem( 'luckyIds' );
  if ( cache === '' ) {
    localStorage.setItem( 'luckyIds', number );
    return true;
  }
  const arrCache = cache.split( ',' );
  if ( arrCache.includes( number ) ) {
    return false;
  }
  cache += `,${ number }`;
  localStorage.setItem( 'luckyIds', cache );
  return true;
}

const renderizar = ( pokemon ) => {
  dadosPokemon.className = 'col col-12 col-md-6 center result-section';

  const pokebolaImage = document.getElementById( 'pokebola-div' );
  pokebolaImage.className = 'hide';

  inputBuscaPorId.value = pokemon.id;

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = pokemon.nome;

  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = `ID: ${ pokemon.id }`;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Peso: ${ pokemon.peso }`;

  const headerTipo = dadosPokemon.querySelector( '.header-tipo' );
  headerTipo.innerHTML = pokemon.tipos.length === 1 ? 'Tipo' : 'Tipos';

  const tipoUl = dadosPokemon.querySelector( '.tipo' );
  tipoUl.innerHTML = '';

  pokemon.tipos.forEach( tipo => {
    const novoLi = document.createElement( 'li' );
    novoLi.className = 'result-section-li';
    const conteudoLi = document.createTextNode( tipo.type.name );
    novoLi.appendChild( conteudoLi );
    tipoUl.appendChild( novoLi );
  } );


  dadosPokemon.querySelector( '.header-estatisticas' ).innerHTML = 'Estatisticas';

  const estatisticaUl = dadosPokemon.querySelector( '.estatistica' );
  estatisticaUl.innerHTML = '';

  pokemon.estatisticas.forEach( estatistica => {
    const novoLi = document.createElement( 'li' );
    novoLi.className = 'result-section-li';
    const conteudoLi = document.createTextNode( `${ estatistica.stat.name }: ${ estatistica.base_stat }` );
    novoLi.appendChild( conteudoLi );
    estatisticaUl.appendChild( novoLi );
  } );

  dadosPokemon.className += ' result-section';
}

const clearPokemon = () => {
  const pokebolaImage = document.getElementById( 'pokebola-div' );
  pokebolaImage.className = '';

  inputBuscaPorId.value = '';
  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = '';
  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = '';
  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = '';
  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = '';
  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = '';
  const headerTipo = dadosPokemon.querySelector( '.header-tipo' );
  headerTipo.innerHTML = '';
  const tipoUl = dadosPokemon.querySelector( '.tipo' );
  tipoUl.innerHTML = '';

  dadosPokemon.querySelector( '.header-estatisticas' ).innerHTML = '';

  const estatisticaUl = dadosPokemon.querySelector( '.estatistica' );
  estatisticaUl.innerHTML = '';

  const mensagemErro = document.getElementById( 'mensagem-de-erro' );
  mensagemErro.className = 'mensagem-escondida';
  dadosPokemon.className = 'col col-12 col-md-6 center result-section hide-on-mobile';
}

const renderizarPokemonBuscado = () => {
  const idAtual = getIdOnScreen;
  const number = inputBuscaPorId.value;

  if ( idAtual === number ) {
    return;
  }
  if ( number > 0 && number < 893 ) {
    pokeApi.buscarEspecífico( number )
      .then( pokemon => {
        const poke = new Pokemon( pokemon );
        renderizar( poke );
      } );
  } else {
    if ( number === '' ) {
      clearPokemon();
      return;
    }
    const mensagemErro = document.getElementById( 'mensagem-de-erro' );
    mensagemErro.className = '';
    setTimeout( () => {
      mensagemErro.className = 'mensagem-escondida';
    }, 3000 );
    return;
  }

  scrollToTheEnd();
}

const feelingLuckyAction = () => {
  let pokemonNumber;
  const idAtual = getIdOnScreen;

  let gerarNovoNumero = true;

  while ( gerarNovoNumero ) {
    pokemonNumber = Math.round( Math.random( ) * ( 893 - 1 ) + 1 );
    if ( idAtual !== pokemonNumber ) {
      if ( podeBuscarComSorte( pokemonNumber ) ) {
        gerarNovoNumero = false;
      }
    }
  }

  pokeApi.buscarEspecífico( pokemonNumber )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke );
    } );

  scrollToTheEnd();
}

inputBuscaPorId.addEventListener( 'blur', renderizarPokemonBuscado );
feelingLuckyButton.addEventListener( 'click', feelingLuckyAction );
clearButton.addEventListener( 'click', clearPokemon );
voltarParaTopoButton.addEventListener( 'click', scrollToTheTop );
pokebolaImages.forEach( image => {
  image.addEventListener( 'click', feelingLuckyAction );
} );
