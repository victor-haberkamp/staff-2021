class PokeApi { // eslint-disable-line no-unused-vars
  constructor() {
    this._url = 'https://pokeapi.co/api/v2/pokemon';
  }

  buscarTodos() {
    const requisicao = fetch( `${ this._url }?limit=100$offset=200` );
    return requisicao.then( data => data.json() ).then( data => data.results );
  }

  buscarEspecífico( id ) {
    const requisicao = fetch( `${ this._url }/${ id }` );
    return requisicao.then( data => data.json() );
  }
}
