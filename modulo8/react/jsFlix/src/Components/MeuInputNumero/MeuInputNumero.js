import React, { Component } from 'react';

export default class MeuInputNumero extends Component {
  
  perderFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props;
    const nota = evt.target.value;
    const erro = obrigatorio && !nota;
    atualizarValor( { erro, nota } );
  }

  render() {
    const { placeholder, exibir, exibirErro, mensagem, obrigatorio} = this.props;

    return  exibir && (
      <React.Fragment>
        {mensagem && <span>{mensagem}</span>}
        <input className={ exibirErro ? 'erro': '' } type="number" placeholder={placeholder} onBlur={ this.perderFoco } />
        { obrigatorio && <span className="mensagem-erro" >*obrigatorio*</span> }
      </React.Fragment> );
  }

}