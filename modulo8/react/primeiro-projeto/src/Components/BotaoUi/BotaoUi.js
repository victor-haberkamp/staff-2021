import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './botaoUi.css'

export default class BotaoUi extends Component{
  
  render( ) {
    const { onClick, className, text, link } = this.props;

    return( 
      <React.Fragment>
        <button className={`btn ${className}`} onClick={onClick}>{ link ? <Link
          className='link'  
          to={link}
        >{ text }</Link> : text}</button>
      </React.Fragment>
      )
  }
 
}

BotaoUi.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func,
}