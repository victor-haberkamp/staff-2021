import React from 'react';
import { Link } from 'react-router-dom';

const ListaEpisodios = ( { listaEpisodios } ) =>
  <React.Fragment>
    {
      listaEpisodios && listaEpisodios.map( ( e, i ) =>
        <li key={ i }>
          <Link to={{ pathname: `/detalhes/${ e.id }`, state: { episodio: e } }}>
            { `${ e.nome } - ${ e.nota || 'Sem Nota' } - ${ e.dataEstreia || 'N/D' }` }
          </Link>
        </li>
      )
    }
  </React.Fragment>

  export default ListaEpisodios;