import React, { Component } from 'react';

import BotaoUi from '../../Components/BotaoUi';
/*
const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment>
      

      {
        listaEpisodios.avaliados && (
          listaEpisodios.avaliados.map( ( e, i ) => {
            return <li key={ i }>{ `${ e.nome } - ${ e.nota }` }</li>;
          })
        )
      }
    </React.Fragment>
  );
} 

export default ListaAvaliacoes;
*/


export default class ListaAvaliacoes extends Component {

  renderEpisodes = ( props ) => {
    const { listaEpisodios } = props.location.state;
    const episodes = [];
    
    listaEpisodios.todos.forEach( episodio => {
      if( episodio.nota != 0 ) {
        episodes.push(<BotaoUi className='verde' text={`${episodio.nome}`} link={ { pathname: `/episodio/${ episodio.id }`, state: { episodio } } } />);
      }
    });
       
    return (
      <div>
        { episodes }
      </div>
    );

  }

  orderEpisodesBySeason( episodes ) {
    episodes.sort( ( a, b ) => {
      const n = a.temporada - b.temporada;
      if( n !== 0  ) {
        return n;
      }
      return a.ordem > b.ordem ;
    } );
    return episodes;
  }

  render() {

    return ( 

      <div>
        <BotaoUi className='verde' link={ { pathname: "/" } } text="Página Inicial" />
        <h1>Listagem Avaliações</h1>
        { this.renderEpisodes( this.props ) }
      </div>
    ); 
  }

}

