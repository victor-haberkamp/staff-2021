import React, { useState, useEffect } from 'react';
import EpisodioAPI from '../../models/EpisodioAPI';

function Formulario( props ) {
	const [ qtdVezesAssistido, setQtdVezesAssistido  ] = useState( 1 );
	const [ nome, setNome ] = useState('vitoca');
	const [ idFilme, setIdFilme ] = useState( 0 );
	const [ notas, setNotas ] = useState( [] );

	useEffect( () => {
		let episodioApi = new EpisodioAPI();
		episodioApi.buscarNota( idFilme ).then( e => setNotas( e ) );		
		document.title = `MEEEEU: ${ qtdVezesAssistido }`;
	}, [ idFilme ] );

	return (
		<React.Fragment>
			<h1>Quantidade de Vezes assistido: { qtdVezesAssistido }</h1>
			<button type="button" onClick={ () => setQtdVezesAssistido( qtdVezesAssistido + 1 ) }>Já Assisti</button>
			<input type="text" onBlur={ evt => setNome( evt.target.value ) }></input>
			<p>{ nome }</p>
			<input type="text" onBlur={ evt => setIdFilme( evt.target.value ) }/>
			{ notas.map( e => <h5>{ e.nota }</h5> ) }
		</React.Fragment>
	);

}

export default Formulario;