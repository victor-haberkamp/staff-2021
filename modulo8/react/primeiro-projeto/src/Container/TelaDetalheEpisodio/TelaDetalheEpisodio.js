import React, { Component } from 'react';
import BotaoUi from '../../Components/BotaoUi';
import EpisodioAPI from '../../models/EpisodioAPI';

export default class TelaDetalheEpisodio extends Component {
  constructor(props) {
    super(props);
    this.infoCompleta = [];
    this.carregado = false;
    this.episodio = [];
  }

  componentDidMount() {
    const episodioApi = new EpisodioAPI();
    episodioApi.episodioDetalhes(this.episodio.id).then(e => {
      console.log(e)
      this.infoCompleta = (
        <React.Fragment>
          <h1>{ this.episodio.nome}</h1>
          <img src={ this.episodio.imagem} alt={ this.episodio.nome}></img>
          <br/><span>Temporada/Episodio: { this.episodio.temporadaEpisodio}</span>
          <br/><span>Duracao: { this.episodio.duracao }min</span>
          <br/><span>{ e.sinopse }</span>
          <br/><span>Data de Estreia: { this.dateFormater(e.dataEstreia) }</span>
          <br/><span>Nota: { this.episodio.nota.toFixed(2) }</span>
          <br/><span>{ (e.notaImdb / 10 * 5).toFixed(2) }</span>
        </React.Fragment>
      );
      this.setState(state => { return { ...state, carregado: true } })
    });
  }

  dateFormater( date ) {
    return `${ date.substring( 8, 10 ) }/${ date.substring( 5, 7 ) }/${ date.substring( 0, 4 ) }`;
  }

  renderEpisode(props) {
    const { episodio } = this.props.location.state;

    return (
      this.carregado ?
        <h1>vamo baixa a bolinha ai</h1> :
        (
          <div>
            {this.infoCompleta}
          </div>
        )
    );
  }

  render() {
    const { state } = this.props.location;

    if (state) {
      this.episodio = state.episodio;
    }

    return (

      <div>
        <BotaoUi className='verde' link={{ pathname: "/" }} text="Página Inicial" />
        <h1>Episodio</h1>
        { state ? this.renderEpisode(this.props) : <h2>Nothing to see</h2>}

      </div>
    );
  }
}
