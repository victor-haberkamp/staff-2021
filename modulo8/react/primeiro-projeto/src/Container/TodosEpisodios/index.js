import React, { Component } from 'react';

import EpisodioApi from '../../models/EpisodioAPI';
import ListaEpisodios from '../../Components/ListaEpisodios';
//import BotaoUi from '../../Components/BotaoUi';

export default class TodosEpisodios extends Component {
  constructor(props) {
    super(props);
    this.episodioApi = new EpisodioApi();
    this.state = {
      ordenacao: () => { },
      tipoOrdenacaoDataEstreia: 'ASC',
      tipoOrdenacaoDuracao: 'ASC'
    }
  }

  componentDidMount() {
    const { listaEpisodios } = this.props.location.state;
    const requisicoes = listaEpisodios.todos.map(e => this.episodioApi.buscarDetalhes(e.id));
    Promise.all(requisicoes).then(resultado => {
      listaEpisodios.todos.forEach(episodio => {
        /* resultado.map( e => {
          episodio.dataEstreia = e.find( e => e.id === episodio.id ).dataEstreia
        } ) */
        //episodio.dataEstreia = resultado.find( e => e.id === episodio.id ).dataEstreia
      })
    })
  }

  alterarOrdenacaoParaDataEstreia = () => {
    const { tipoOrdenacaoDataEstreia } = this.state;
    this.setState({
      ordenacao: ( a, b ) => new Date ( ( tipoOrdenacaoDataEstreia === 'ASC' ? a : b ).dataEstreia )
             - new Date ( ( tipoOrdenacaoDataEstreia === 'ASC' ? b : a ).dataEstreia ),
      tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === 'ASC' ? 'DESC' : 'ASC'
    });
  }

  alterarOrdenacaoParaDuracao = () => {
    const { tipoOrdenacaoDuracao } = this.state;
    this.setState({
      ordenacao: ( a, b ) => ( tipoOrdenacaoDuracao === 'ASC' ? a : b ).dataEstreia - ( tipoOrdenacaoDuracao === 'ASC' ? b : a ).dataEstreia,
      tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === 'ASC' ? 'DESC' : 'ASC'
    });
  }

  render() {
    const listaEpisodios = this.props.location.state.listaEpisodios.todos.concat([]);
    listaEpisodios.sort(this.state.ordenacao);

    return (
      <React.Fragment>
        <header className="App-header">
          <h1>Todos Episódios</h1>
          <ListaEpisodios listaEpisodios={listaEpisodios} />
        </header>
      </React.Fragment>
    )
  }

}
