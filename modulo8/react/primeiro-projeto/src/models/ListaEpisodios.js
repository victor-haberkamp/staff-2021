import Episodio from "./Episodio";

function sortear( min, max ) {
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

export default class ListaEpisodios {
  constructor( episodios ) {
    this.todos = episodios.map( episodio => {
      return new Episodio( episodio );
    } )
  }

  get episodioAleatorio() {
    const indice = sortear( 0, this.todos.length );
    return this.todos[ indice ];
  }

  get avaliados() {
    return this.todos.filter( e => e.nota );
  }

}